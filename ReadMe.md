-------------------------------------------------------------------------------
# Platform1 - Simple **Game Engine** prototype for a 2D Platform Game, made with pure C++ & OpenGL and some basic tools.

Code and class description here (in Platform1Demo/Platform1/): https://bitbucket.org/ppp225/platform1demo/src/master/Platform1/

Compiled for windows in the downloads section (requires OpenGL 3.3 support and vcredist to work): https://bitbucket.org/ppp225/platform1demo/downloads

## Goal of the project was to increase the proficiency in:
* C++,
* Topics of code engineering in the context of game design,
* Optimizing the efficiency of displaying objects on screen using OpenGL.

## It's been written with the following in mind:
* Maximum efficiency.
    * Physics and render steps have been optimised in a way that there's no difference
      between rendering the whole current game and a simple static image.
    * Furthermore, there's no lag in player input handling (which is often an issue with interfaces).
* Modular design.
    * Ability to expand the codebase easily.

## The project let me broaden my horizons in the planned fields and will be continued in the future.

* Examples:
    * Adding more game elements.
    * Implementing graphics using tools like Vulkan, DirectX.
    * Using external libraries, (like a physics engine,) to compare performance.