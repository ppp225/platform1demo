#pragma once
#include "SpriteRenderer.h"
#include "GameEntities.h"
#include "TextureStore.h"

// --------------------- xxxxxx ---------------------

class SimpleInstancedTileContainer
{
public:
	std::vector<glm::vec3> ObjectPositions; // instanced. x, y, tex

	void InitRenderer(Shader& shader, UBOBindingWorks& uboBindings, Texture2D& texture, glm::ivec2& textureDivisor, glm::vec4& color) {
		this->Renderer.Init(shader, uboBindings, this->ObjectPositions, texture, textureDivisor, color);
	};
	void Delete() {
		Renderer.Delete();
	};
	void Render() {
		this->Renderer.DrawInstanced();
	};

private:
	InstancedRenderer Renderer;
};

// --------------------- xxxxxx ---------------------

class DynamicObjectContainer
{
public:
	void InitRenderer(Shader& shader, UBOBindingWorks& uboBindings, Texture2D& texture, glm::ivec2& textureDivisor, glm::vec4& color) {
		this->Renderer.Init(shader, uboBindings, texture, textureDivisor, color);
	};
	void Delete() {
		Renderer.Delete();
	};
	void UpdateData(glm::vec4& color, glm::vec2& position, glm::vec2& size, GLuint texNum) {
		this->Renderer.UpdateData(color, position, size, texNum);
	};
	void UpdateData(glm::vec2& position, GLuint texNum) {
		this->Renderer.UpdateData(position, texNum);
	};
	void Render() {
		this->Renderer.Draw();
	};

private:
	DynamicRenderer Renderer;
};

// --------------------- xxxxxx ---------------------

class ParallaxContainer
{
public:
	void InitRenderer(Shader& shader, UBOBindingWorks& uboBindings, Texture2D& texture) {
		this->Renderer.Init(shader, uboBindings, texture);
	};
	void Delete() {
		Renderer.Delete();
	};
	void UpdateData(glm::vec4& color, glm::vec2& position, glm::vec2& size) {
		this->Renderer.UpdateData(color, position, size);
	};
	void UpdateData(glm::vec2& position) {
		this->Renderer.UpdateData(position);
	};
	void Render() {
		this->Renderer.Draw();
	};

private:
	ParallaxRenderer Renderer;
};

// --------------------- xxxxxx ---------------------

class RenderEngine
{
public:
	RenderEngine() = default;
	~RenderEngine();
	void Init(TheWorld& world, UBOBindingWorks& uboBindings);
	void UpdateGLBufferedAssetResolution();
	void Render();

private:
	TheWorld* WorldPointer;

	TextureStore TextureStore;
	std::vector<DynamicObjectContainer> PlayerObjectContainers;
	std::vector<SimpleInstancedTileContainer> RenderTileContainers;
	std::vector<ParallaxContainer> ParallaxContainers;
};

