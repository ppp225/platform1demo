#include "stdafx.h"
#include "InputComponent.h"

// ---------------------------------------------------------------------------------------- Misc ----------------------------------------------------------------------------------------

PlayerInput::PlayerInput() : Left(GL_FALSE), Right(GL_FALSE), Up(GL_FALSE), Down(GL_FALSE), Crouch(GL_FALSE)
{
}

// ---------------------------------------------------------------------------------------- Input Component ----------------------------------------------------------------------------------------

InputComponent::InputComponent()
{
}


PlayerInputComponent::PlayerInputComponent(SettingStore& settingStore) : InputComponent(), SettingStorePointer(&settingStore)
{
}

void PlayerInputComponent::OnKeyInput(GLuint key, GLuint scancode, GLuint action, GLuint mods)
{
	if (action == obw::Action::PRESS) {
		if (key == SettingStorePointer->GetKey(GameAction::Player1LEFT))
			this->Input.Left = GL_TRUE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1RIGHT))
			this->Input.Right = GL_TRUE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1UP))
			this->Input.Up = GL_TRUE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1DOWN))
			this->Input.Down = GL_TRUE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1CROUCH))
			this->Input.Crouch = GL_TRUE;
	}
	else if (action == obw::Action::RELEASE) {
		if (key == SettingStorePointer->GetKey(GameAction::Player1LEFT))
			this->Input.Left = GL_FALSE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1RIGHT))
			this->Input.Right = GL_FALSE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1UP))
			this->Input.Up = GL_FALSE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1DOWN))
			this->Input.Down = GL_FALSE;
		else if (key == SettingStorePointer->GetKey(GameAction::Player1CROUCH))
			this->Input.Crouch = GL_FALSE;
	}
}