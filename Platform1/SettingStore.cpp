#include "stdafx.h"
#include "SettingStore.h"

SettingStore::SettingStore() : KeyMapping(10)
{
	RestoreDefualtKeys();
}


SettingStore::~SettingStore()
{
}

GLint SettingStore::GetKey(GameAction action)
{
	return KeyMapping[action];
}

void SettingStore::ChangeKeyBinding(GameAction action, GLint key)
{
	// remove current binding
	for (auto it : KeyMapping) 
	{
		if (it.second == key) 
		{
			KeyMapping[it.first] = GLFW_KEY_UNKNOWN;
			break;
		}
	}
	// bind new key
	KeyMapping[action] = key;
}

void SettingStore::RestoreDefualtKeys()
{
	KeyMapping[GameAction::Player1UP] = GLFW_KEY_W;
	KeyMapping[GameAction::Player1DOWN] = GLFW_KEY_S;
	KeyMapping[GameAction::Player1LEFT] = GLFW_KEY_A;
	KeyMapping[GameAction::Player1RIGHT] = GLFW_KEY_D;
	KeyMapping[GameAction::Player1CROUCH] = GLFW_KEY_LEFT_CONTROL;

	KeyMapping[GameAction::Pause] = GLFW_KEY_P;
	KeyMapping[GameAction::Restart] = GLFW_KEY_F5;
	KeyMapping[GameAction::Options] = GLFW_KEY_F1;

	KeyMapping[GameAction::Fullscreen] = GLFW_KEY_F11;
	KeyMapping[GameAction::CloseApp] = GLFW_KEY_ESCAPE;
}
