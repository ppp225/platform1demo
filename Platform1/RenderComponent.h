#pragma once
#include "PositionComponent.h"

// --------------------- Render ---------------------

class RenderComponent
{
	friend class RenderEngine;	// so it can read all private members - getters are useless in this case

public:
	enum RenderingType
	{
		STATIC,
		DYNAMIC,
		PARALLAX,
	};
	RenderComponent(PositionComponent& positionComponent, RenderingType type, GLuint textureID, GLuint textureNum, glm::vec4 color);

	/// <summary> Returns current 'TextureNum' </summary>
	GLuint GetTextureNum();
	/// <summary> Returns TRUE if 'TextureNum' changed </summary>
	GLboolean SetTextureNum(GLuint textureNum);
	/// <summary> Use 'SetTextureNum' to know if 'TextureNum' changed </summary>
	void SetTextureNumFast(GLuint textureNum);

private:
	PositionComponent* PositionComponentPointer;

	GLuint TextureID;	// texture id (ex. 'player-sprite.png')
	GLuint TextureNum;	// texture num (ex. #5 (Jumping))
	glm::vec4 Color;



	RenderingType Type;	// TODO: not used currently

};