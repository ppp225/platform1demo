#pragma once

#include <vector>

class TileTools
{
public:
	// var
	static const GLint TileConvolutionLength = 21;

	// fx
	static GLuint GetTileType(GLuint x, GLuint y, std::vector<std::vector<GLuint>>& tileData);

private:
	// fx
	static GLboolean CompareTC(GLboolean d1[9], const GLint d2[9]);

	// arrays
	static const GLint TileConvolution[TileConvolutionLength][9];
	static const GLint TileOrderHashtable[TileConvolutionLength];
};

