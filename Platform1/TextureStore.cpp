#include "stdafx.h"
#include "TextureStore.h"

TextureStore::TextureStore()
{
	this->TextureFilenames[1] = "Data/Textures/8px/100.png";
	this->TextureDivisors[1] = glm::vec2(7, 3);

	this->TextureFilenames[999] = "Data/Textures/8px/player-sheet.png";
	this->TextureDivisors[999] = glm::vec2(7, 5);

	this->TextureFilenames[TextureNames::PARALLAX1] = "Data/Textures/8px/Level1/par1.png";
	this->TextureFilenames[TextureNames::PARALLAX2] = "Data/Textures/8px/Level1/par2.png";
	this->TextureFilenames[TextureNames::PARALLAX3] = "Data/Textures/8px/Level1/par3.png";
}

TextureStore::~TextureStore()
{
	ClearTextures();
}

void TextureStore::ClearTextures()
{
	for (auto& var : Textures)
		var.second.Delete();
	this->Textures.clear();
}

Texture2D& TextureStore::GetTexture(GLuint id)
{
	if (this->TextureFilenames.count(id) == 0)
		Logger::Log("ERROR::TEXTURE_STORAGE::no such texture.");

	if (this->Textures.count(id) == 0)
		this->Textures[id] = Texture2D::loadTextureFromFile(this->TextureFilenames[id], GL_TRUE);

	return this->Textures[id];
}

glm::ivec2 TextureStore::GetTextureDivisor(GLuint id)
{
	return this->TextureDivisors[id];
}

// ---------------------------------------------------------------------------------------- xxxxxx ----------------------------------------------------------------------------------------

PlayerTextureStore::PlayerTextureStore()
{
	this->Hitboxes[PlayerTextureID::STAND_R] = glm::vec4(0, 0, 1, 2);			// 0
	this->Hitboxes[PlayerTextureID::STAND_L] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::STAND_DR] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::STAND_DL] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::STAND_UR] = glm::vec4(0, 0, 1, 2);

	this->Hitboxes[PlayerTextureID::LAND_R_33] = glm::vec4(0, 0, 1, 1.875f);	// 5
	this->Hitboxes[PlayerTextureID::LAND_R_66] = glm::vec4(0, 0, 1, 1.75f);
	this->Hitboxes[PlayerTextureID::LAND_R_100] = glm::vec4(0, 0, 1, 1.5f);

	this->Hitboxes[PlayerTextureID::LAND_L_33] = glm::vec4(0, 0, 1, 1.875f);
	this->Hitboxes[PlayerTextureID::LAND_L_66] = glm::vec4(0, 0, 1, 1.75f);
	this->Hitboxes[PlayerTextureID::LAND_L_100] = glm::vec4(0, 0, 1, 1.5f);		// 10

	this->Hitboxes[PlayerTextureID::CROUCH_R_33] = glm::vec4(0, 0, 1, 1.875f);
	this->Hitboxes[PlayerTextureID::CROUCH_R_66] = glm::vec4(0, 0, 1, 1.75f);
	this->Hitboxes[PlayerTextureID::CROUCH_R_100] = glm::vec4(0, 0, 1, 1.5f);	// 13

	this->Hitboxes[PlayerTextureID::CROUCH_L_33] = glm::vec4(0, 0, 1, 1.875f);	// 14
	this->Hitboxes[PlayerTextureID::CROUCH_L_66] = glm::vec4(0, 0, 1, 1.75f);	// 19
	this->Hitboxes[PlayerTextureID::CROUCH_L_100] = glm::vec4(0, 0, 1, 1.5f);	// 24

	this->Hitboxes[PlayerTextureID::RISE_R_50] = glm::vec4(0, 0.25f, 1, 2);		// 15
	this->Hitboxes[PlayerTextureID::RISE_R_100] = glm::vec4(0, 0.25f, 1, 2);
	this->Hitboxes[PlayerTextureID::FALL_R_50] = glm::vec4(0, 0.125f, 1, 2);
	this->Hitboxes[PlayerTextureID::FALL_R_100] = glm::vec4(0, 0.25f, 1, 2);

	this->Hitboxes[PlayerTextureID::RISE_L_50] = glm::vec4(0, 0.25f, 1, 2);		// 20
	this->Hitboxes[PlayerTextureID::RISE_L_100] = glm::vec4(0, 0.25f, 1, 2);
	this->Hitboxes[PlayerTextureID::FALL_L_50] = glm::vec4(0, 0.125f, 1, 2);
	this->Hitboxes[PlayerTextureID::FALL_L_100] = glm::vec4(0, 0.25f, 1, 2);

	// future TODO: update move animations
	this->Hitboxes[PlayerTextureID::MOVE_R_0] = glm::vec4(0, 0, 1, 2);			// 25
	this->Hitboxes[PlayerTextureID::MOVE_R_1] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::MOVE_R_2] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::MOVE_R_3] = glm::vec4(0, 0, 1, 2);
	//this->Hitboxes[PlayerTextureID::UNDEF] = glm::vec4(0, 0, 1, 2);

	this->Hitboxes[PlayerTextureID::MOVE_L_0] = glm::vec4(0, 0, 1, 2);			// 30
	this->Hitboxes[PlayerTextureID::MOVE_L_1] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::MOVE_L_2] = glm::vec4(0, 0, 1, 2);
	this->Hitboxes[PlayerTextureID::MOVE_L_3] = glm::vec4(0, 0, 1, 2);
	//this->Hitboxes[PlayerTextureID::UNDEF] = glm::vec4(0, 0, 1, 2);
}

PlayerTextureStore::~PlayerTextureStore()
{
}

glm::vec4 PlayerTextureStore::GetHitbox(GLuint id)
{
	std::unordered_map<GLuint, glm::vec4>::const_iterator val = Hitboxes.find(id);

	if (val == Hitboxes.end())
		Logger::Log("ERROR::PlayerTextureStore::GetHitbox::illegal id");
	else
		return val->second;
}
