#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <vector>

class SimpleCamera
{
public:
	// attributes
	glm::vec3 Position;
	glm::vec3 Front;
	glm::vec3 Up;
	// options
	GLfloat Zoom;
	// position corrections
	glm::vec2 Min;
	glm::vec2 Max;
	glm::vec2 RegularViewCorrection;

	SimpleCamera()
	{
		this->Position = glm::vec3(0.0f, 0.0f, 0.0f);
		this->Front = glm::vec3(0.0f, 0.0f, -1.0f);
		this->Up = glm::vec3(0.0f, 1.0f, 0.0f);
		this->Zoom = 1.0f;
	}

	// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
	glm::mat4 GetViewMatrix(GLfloat scale)
	{
		return glm::lookAt(this->Position * scale, (this->Position * scale) + this->Front, this->Up);
	}

	void UpdatePosition(glm::vec2 newPos)
	{
		this->Position.x = newPos.x + RegularViewCorrection.x;
		this->Position.y = newPos.y + RegularViewCorrection.y;

		if (this->Position.x < Min.x)
			this->Position.x = Min.x;
		if (this->Position.y < Min.y)
			this->Position.y = Min.y;

		if (this->Position.x > Max.x)
			this->Position.x = Max.x;
		if (this->Position.y > Max.y)
			this->Position.y = Max.y;

		//std::cout << "\r" << "CamPos: " << this->Position.x << " | " << this->Position.y;
	}
};