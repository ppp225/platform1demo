#pragma once
#include "Texture2D.h"

#include <unordered_map>

// --------------------- TextureStore ---------------------

class TextureStore
{
	enum TextureNames {
		PARALLAX1 = 1000,
		PARALLAX2 = 1001,
		PARALLAX3 = 1002,
	};
public:
	TextureStore();
	~TextureStore();
	void ClearTextures();
	Texture2D& GetTexture(GLuint id);
	glm::ivec2 GetTextureDivisor(GLuint id);

private:
	std::unordered_map<GLuint, const char*> TextureFilenames;
	std::unordered_map<GLuint, glm::vec2> TextureDivisors;
	std::unordered_map<GLuint, Texture2D> Textures;
};

// --------------------- PlayerTextureStore ---------------------

class PlayerTextureStore
{
public:
	enum PlayerTextureID {
		// STAND	 |  L	A	N	D		R	L	D |	RISE FALL R	   | RISE FALL L	|	 MOVE R	   | MOVE L
		STAND_UR = 4, LAND_L_66 = 9, CROUCH_L_33 = 14, CROUCH_L_66 = 19, CROUCH_L_100 = 24, UNDEFIN4 = 29, UNDEFIN5 = 34,
		STAND_DL = 3, LAND_L_33 = 8, CROUCH_R_100 = 13, FALL_R_100 = 18, FALL_L_100 = 23, MOVE_R_3 = 28, MOVE_L_3 = 33,
		STAND_DR = 2, LAND_R_100 = 7, CROUCH_R_66 = 12, FALL_R_50 = 17, FALL_L_50 = 22, MOVE_R_2 = 27, MOVE_L_2 = 32,
		STAND_L = 1, LAND_R_66 = 6, CROUCH_R_33 = 11, RISE_R_100 = 16, RISE_L_100 = 21, MOVE_R_1 = 26, MOVE_L_1 = 31,
		STAND_R = 0, LAND_R_33 = 5, LAND_L_100 = 10, RISE_R_50 = 15, RISE_L_50 = 20, MOVE_R_0 = 25, MOVE_L_0 = 30,
	};
	
public:
	PlayerTextureStore();
	~PlayerTextureStore();
	glm::vec4 GetHitbox(GLuint id);

private:
	std::unordered_map<GLuint, glm::vec4> Hitboxes;	// <vec2 min, vec2 max>
};