#include "stdafx.h"
#include "TileTools.h"

/**
TileType correspond to textures. Lets say textures are packed like this in files:
2 5 8 11 13
1 4 7 10 14
0 3 6 09 12 15 18
*/

GLboolean TileTools::CompareTC(GLboolean d1[9], const GLint d2[9])
{
	for (GLuint i = 0; i < 9; ++i)
	{
		if (d2[i] == 2)
			continue;
		if (d2[i] != d1[i])
			return GL_FALSE;
	}
	return GL_TRUE;
}

GLuint TileTools::GetTileType(GLuint x, GLuint y, std::vector<std::vector<GLuint>>& tileData)
{
	GLint d1 = 0, d2 = 0, d3 = 0, d4 = 0, d5 = 0, d6 = 0, d7 = 0, d8 = 0, d9 = 0;
	GLuint xMax = tileData[0].size() - 1, yMax = tileData.size() - 1;

	if (x > 0 && y > 0 && x < xMax && y < yMax)
	{
		d1 = tileData[y + 1][x - 1], d2 = tileData[y + 1][x], d3 = tileData[y + 1][x + 1],
			d4 = tileData[y][x - 1], d5 = tileData[y][x], d6 = tileData[y][x + 1],
			d7 = tileData[y - 1][x - 1], d8 = tileData[y - 1][x], d9 = tileData[y - 1][x + 1];
	}
	else
	{
		d5 = tileData[y][x];

		if (0 < x)
		{
			d4 = tileData[y][x - 1];
			if (y < yMax)
				d1 = tileData[y + 1][x - 1];
			if (0 < y)
				d7 = tileData[y - 1][x - 1];
		}
		if (x < xMax)
		{
			d6 = tileData[y][x + 1];
			if (y < yMax)
				d3 = tileData[y + 1][x + 1];
			if (0 < y)
				d9 = tileData[y - 1][x + 1];
		}
		if (y < yMax)
			d2 = tileData[y + 1][x];
		if (0 < y)
			d8 = tileData[y - 1][x];
	}

	{
		GLint TT2[9] =
		{
			d1, d2, d3,
			d4, d5, d6,
			d7, d8, d9
		};
		for (GLuint i = 0; i < 9; ++i) {
			if (TT2[i] == 999)
				TT2[i] = 0;
		}
		GLboolean TT[9] =
		{
			(GLboolean)TT2[0], (GLboolean)TT2[1], (GLboolean)TT2[2],
			(GLboolean)TT2[3], (GLboolean)TT2[4], (GLboolean)TT2[5],
			(GLboolean)TT2[6], (GLboolean)TT2[7], (GLboolean)TT2[8]
		};
		for (GLuint i = 0; i < TileConvolutionLength; ++i)
		{
			GLint hash = TileOrderHashtable[i];
			if (CompareTC(TT, TileConvolution[hash]))
				return hash;
			//memcmp(TileConvolution[i], TT, sizeof(TT)) == 0
		}
	}
	return 20;
}

// determines the order of tile checking. Tile 4 is at the end, so tiles 9, 11, 15, 17 have a chence to be picked
const GLint TileTools::TileOrderHashtable[TileConvolutionLength] =
{
	0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 4, 20
};

const GLint TileTools::TileConvolution[TileConvolutionLength][9] =
{
	{//0 column 0
		2,1,2,
		0,1,1,
		2,0,2
	},
	{
		2,1,2,
		0,1,1,
		2,1,2
	},
	{
		2,0,2,
		0,1,1,
		2,1,2
	},
	{//3
		2,1,2,
		1,1,1,
		2,0,2
	},
	{
		2,1,2,
		1,1,1,
		2,1,2
	},
	{
		2,0,2,
		1,1,1,
		2,1,2
	},
	{//6
		2,1,2,
		1,1,0,
		2,0,2
	},
	{
		2,1,2,
		1,1,0,
		2,1,2
	},
	{
		2,0,2,
		1,1,0,
		2,1,2
	},
	{//9 column 3
		1,1,1,
		1,1,1,
		0,1,1
	},
	{
		0,0,2,
		0,1,1,
		0,0,2
	},
	{
		0,1,1,
		1,1,1,
		1,1,1
	},
	{//12
		2,1,2,
		0,1,0,
		0,0,0
	},
	{
		0,0,0,
		0,1,0,
		0,0,0
	},
	{
		0,0,0,
		0,1,0,
		2,1,2
	},
	{//15
		1,1,1,
		1,1,1,
		1,1,0
	},
	{
		2,0,0,
		1,1,0,
		2,0,0
	},
	{
		1,1,0,
		1,1,1,
		1,1,1
	},
	{//18 column 6
		2,1,2,
		0,1,0,
		2,1,2
	},
	{
		2,0,2,
		1,1,1,
		2,0,2
	},
	{
		2,2,2,
		2,2,2,
		2,2,2
	},
};