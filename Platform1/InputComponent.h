#pragma once
#include "ObserverWorks.h"
#include "SettingStore.h"

// --------------------- Misc ---------------------

class PlayerInput
{
public:
	PlayerInput();
	~PlayerInput() = default;

	GLboolean Left, Right, Up, Down;
	GLboolean Crouch;
};

// --------------------- Input Component ---------------------

class InputComponent
{
public:
	InputComponent();
	virtual ~InputComponent() = default;

	PlayerInput Input;

	virtual void Update(GLfloat dt) = 0;

};

class PlayerInputComponent : public InputComponent, public obw::KeyInputObserver
{
public:
	PlayerInputComponent(SettingStore& settingStore);
	virtual ~PlayerInputComponent() = default;

	SettingStore* SettingStorePointer;

	virtual void Update(GLfloat dt) {};
	virtual void OnKeyInput(GLuint key, GLuint scancode, GLuint action, GLuint mods);
};