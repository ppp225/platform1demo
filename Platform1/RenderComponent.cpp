#include "stdafx.h"
#include "RenderComponent.h"

// ---------------------------------------------------------------------------------------- Render ----------------------------------------------------------------------------------------

RenderComponent::RenderComponent(PositionComponent & positionComponent, RenderingType type, GLuint textureID, GLuint textureNum, glm::vec4 color) :
	PositionComponentPointer(&positionComponent), Type(type), TextureID(textureID), TextureNum(textureNum), Color(color)
{
}

GLuint RenderComponent::GetTextureNum() { return this->TextureNum; }

GLboolean RenderComponent::SetTextureNum(GLuint textureNum)
{
	if (this->TextureNum == textureNum)
		return GL_FALSE;

	this->TextureNum = textureNum;
	return GL_TRUE;
}

void RenderComponent::SetTextureNumFast(GLuint textureNum)
{
	this->TextureNum = textureNum;
}
