#pragma once
#include "ObserverWorks.h"
#include "SpriteRenderer.h"
#include "SettingStore.h"
#include "SettingExecutor.h"
#include "GameEntities.h"
#include "PhysicsEngine.h"
#include "RenderEngine.h"

// game design resolution.
const GLuint GAME_WIDTH = 1920, GAME_HEIGHT = 1080;
// the game uses an internal size. It is scaled by (UNIT_SIZE * scale) to on screen pixels.
// scale is determined in the GameMain class based on the window resolution and passed as an argument in the render method
const GLfloat UNIT_SIZE = 64;

class GameMain
{
public:
	// constructor/destructor
	GameMain(GLuint width, GLuint height);
	~GameMain();
	// game state change
	void Init();	// GL fx
	void SetSubjects(obw::MousePositionSubject& mousePositionSubjectPointer, obw::MouseButtonSubject& mouseButtonSubjectPointer, obw::KeyInputSubject& keyInputSubjectPointer);
	void UpdateResolution(GLuint width, GLuint height);

	// camera state
	glm::mat4 Projection;

	// game state
	GameState State;

	// gameloop
	// returns number of physics steps made
	GLuint Update(GLfloat dt);
	void Render();	// GL fx

	// hacks
	SettingStore& getSS() { return Settings; };

private:
	// vars
	GLuint Width, Height;
	GLfloat Scale;

	// objects
	SettingStore Settings;
	SettingExecutor KeyHandler;

	PhysicsEngine Physics;
	RenderEngine Renderer;
	UBOBindingWorks UBOBindings;

	TheWorld World;
	GameLevel Level;	// TODO: remove this after making it static

	// subjects
	obw::MousePositionSubject* MousePositionSubjectPointer = nullptr;
	obw::MouseButtonSubject* MouseButtonSubjectPointer = nullptr;
	obw::KeyInputSubject* KeyInputSubjectPointer = nullptr;

	// states
	GLboolean updateResolution_ = GL_FALSE;
	GLboolean updateCameraPosition_ = GL_TRUE;	// TODO: is true all the time for now

	// temp vars
	GLuint newWidth, newHeight;

	// state change
	void OnRestart();

	// projection data UBO
	GLuint ProjectionDataUBO;
	void GLInitProjectionDataUBO(glm::mat4& projection, glm::mat4& view, GLfloat scale);
	void GLUpdateProjectionDataUBO(glm::mat4& projection, glm::mat4& view, GLfloat scale);
	void GLUpdateProjectionDataUBO(glm::mat4& view);

	// render help
	void GLUpdateResolution();
};

