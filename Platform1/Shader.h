/************************************************
* Shader
************************************************/
#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
public:
	// init/fini
	Shader() = default;
	~Shader() = default;
	// deletes shader from VRAM
	void Delete();
	// uses current shader and returns a reference
	Shader& Use();
	// static factory function. Returns a compiled shader loaded from specified file paths. Geometry shader is optional.
	static Shader& LoadShader(const GLchar *vShaderFile, const GLchar *fShaderFile, const GLchar *gShaderFile = nullptr);
	// set UBO binding point
	void SetUniformBlockBinding(const GLchar *name, const GLuint binding);

private:
	// state
	GLuint ID = NULL;
	// compiles shader from source code
	void Compile(const GLchar *vertexSource, const GLchar *fragmentSource, const GLchar *geometrySource = nullptr);
	// checks compilation, linking errors and prints logs
	void CheckCompileErrors(GLuint object, std::string type);
	// loads and generates a shader from file
	static Shader& LoadShaderFromFile(const GLchar * vShaderFile, const GLchar * fShaderFile, const GLchar * gShaderFile);
};