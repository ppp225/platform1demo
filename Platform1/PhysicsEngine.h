#pragma once
#include "GameEntities.h"


class PhysicsEngine
{
	const GLboolean DEBUG = GL_TRUE;

public:
	PhysicsEngine();
	~PhysicsEngine();
	void Init(TheWorld& world);
	GLuint Update(GLfloat dt);

private:
	TheWorld* WorldPointer_;

	// fixed physics timestep
	const GLfloat upsTime_ = 1.0f / 250;
	GLfloat accTime_ = 0.0f;

	// collisions
	void DoCollisions();
	GLboolean DetectCollision(glm::vec2& min1, glm::vec2& max1, glm::vec2& min2, glm::vec2& max2);
	glm::vec2 MinimumTransition(glm::vec2& min1, glm::vec2& max1, glm::vec2& min2, glm::vec2& max2);

	// misc
	void UpdateCamera();
	void UpdateParallax();
};

