#include "stdafx.h"
#include "PhysicsComponent.h"

// ---------------------------------------------------------------------------------------- Physics ----------------------------------------------------------------------------------------

PhysicsComponent::PhysicsComponent(PositionComponent & positionComponent) : PositionComponentPointer(&positionComponent), min_(0.0f, 0.0f), max_(positionComponent.Size)
{
}

PhysicsComponent::PhysicsComponent(PositionComponent & positionComponent, glm::vec4 hitbox) : PositionComponentPointer(&positionComponent), min_(hitbox.xy), max_(hitbox.zw)
{
}

const glm::vec2& PhysicsComponent::GetHitboxMin()
{
	return min_;
}

const glm::vec2& PhysicsComponent::GetHitboxMax()
{
	return max_;
}

// --------------------- Player ---------------------

PlayerPhysicsComponent::PlayerPhysicsComponent(PositionComponent & positionComponent, PlayerInputComponent & playerInputComponent, RenderComponent& renderComponent) :
	PhysicsComponent(positionComponent), PlayerInputComponentPointer(&playerInputComponent), RenderComponentPointer(&renderComponent)
{
	State = new StandingState(GL_TRUE);
	this->State->OnEnter(*this);

	RecalcPrecalc();
}

void PlayerPhysicsComponent::Update(GLfloat dt) {
	glm::vec2 temp = this->PositionComponentPointer->Position;

	PlayerState* state = this->State->Update(*this, dt, PlayerInputComponentPointer->Input);
	if (state)
	{
		delete this->State;
		this->State = state;

		// call enter action on new state
		this->State->OnEnter(*this);
	}

	prevPos = temp;
}

void PlayerPhysicsComponent::OnPhysics(GLboolean ground, GLboolean fall)
{
	if (ground)
	{
		if (this->State->ID != PlayerState::StateID::GROUND && this->State->SubID != PlayerState::StateSubID::JUMPING)
		{
			State = new LandingState(this->State->lookRight_);
			this->State->OnEnter(*this);
		}
	}
	else if (fall)
	{
		if (this->State->ID != PlayerState::StateID::AERIAL)
		{
			State = new FallingState(this->State->lookRight_);
			this->State->OnEnter(*this);
		}
	}
}

/// <summary> Updates the <see cref="RenderComponent::TextureNum"/> and this->HITBOX(min_, max_) </summary>
/// <param name="textureNum"><see cref="PlayerTextureStore::PlayerTextureID"/></param>
/// <param name="hitboxChanged">TRUE(default) - Hitbox will be changed. FALSE - it won't be. [Use this for more efficiency.]</param>
void PlayerPhysicsComponent::UpdateTexture(GLuint textureNum, GLboolean hitboxChanged)
{
	if (hitboxChanged) {
		if (this->RenderComponentPointer->SetTextureNum(textureNum))
			HitboxChanged();
	}
	else
		this->RenderComponentPointer->SetTextureNumFast(textureNum);
}

void PlayerPhysicsComponent::HitboxChanged()
{
	glm::vec4 temp = this->PlayerTexture.GetHitbox(this->RenderComponentPointer->GetTextureNum());
	min_ = temp.xy;
	max_ = temp.zw;
}

void PlayerPhysicsComponent::RecalcPrecalc()
{
	GLfloat gravityLen = glm::length(this->Gravity);
	this->PrecalculatedValues.airDrag = this->DragCoefficient * this->Mass * gravityLen;
	this->PrecalculatedValues.gravity = this->Gravity * this->Mass;
	this->PrecalculatedValues.staticFriction = this->FrictionCoefficient * this->Mass * gravityLen;
}

// --------------------- Other ---------------------

StaticPhysicsComponent::StaticPhysicsComponent(PositionComponent & positionComponent) : PhysicsComponent(positionComponent)
{
}

ParallaxPhysicsComponent::ParallaxPhysicsComponent(PositionComponent & positionComponent, GLfloat depth) : PhysicsComponent(positionComponent), Depth(depth)
{
}

void ParallaxPhysicsComponent::UpdatePosition(glm::vec2 camPos) {
	this->PositionComponentPointer->Position = (camPos * this->Depth);
}

// ---------------------------------------------------------------------------------------- Player States : MAIN CLASS ----------------------------------------------------------------------------------------

void PlayerState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	this->ID = StateID::UNDEF;
	this->SubID = StateSubID::UNDEF;
}

PlayerState * PlayerState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	/* README:
	* update usually should have the following form:
	*
	// update tex
	physicsComponent.UpdateTexture(PlayerTexture::STANDING);
	// transition
	if (0) return new PlayerState();
	// super
	return LV-UP_State::Update(physicsComponent, dt, input);
	*
	**/

	// pre
	auto& pos = physicsComponent.PositionComponentPointer->Position;

	// main movement physics - Verlet method
	glm::vec2 acceleration = (Force(physicsComponent, dt, input) + Friction(physicsComponent, dt, input)) / physicsComponent.GetMass();
	pos += dt * (physicsComponent.Velocity + dt * acceleration / 2.0f);
	physicsComponent.Velocity += dt * acceleration;
	glm::vec2 newAcceleration_ = (Force(physicsComponent, dt, input) + Friction(physicsComponent, dt, input)) / physicsComponent.GetMass();
	physicsComponent.Velocity += dt * (acceleration + newAcceleration_) / 2.0f;

	return nullptr;	// toplevel, doesn't change any state
}

// ---------------------------------------------------------------------------------------- Player States : GROUNDED ----------------------------------------------------------------------------------------

inline void GroundedState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	PlayerState::OnEnter(physicsComponent);

	this->ID = StateID::GROUND;
}

inline PlayerState * GroundedState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// input
	if (this->SubID != StateSubID::LANDING)
		if (input.Up && input.Down)
			;
		else if (input.Up)
			return new JumpingState(lookRight_);
		else if (input.Crouch && this->SubID != StateSubID::CROUCHING)
			return new CrouchingState(lookRight_);
	
	// transition
	if (physicsComponent.prevPos.y > physicsComponent.PositionComponentPointer->Position.y)
		return new FallingState(lookRight_);

	// corrections
	GLfloat len = (physicsComponent.Velocity.x * physicsComponent.Velocity.x);
	if (len < 0.01f)
		physicsComponent.Velocity *= 0.0f;

	// super
	return PlayerState::Update(physicsComponent, dt, input);
}

glm::vec2 GroundedState::Force(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// pre
	glm::vec2 force(physicsComponent.MoveForce, 0.0f);
	glm::vec2 directionVector(0.0f, 0.0f);

	// body
	if (input.Left && input.Right)
		;
	else if (input.Left)
		directionVector.x = -1;
	else if (input.Right)
		directionVector.x = 1;

	force *= directionVector;

	return force;
}

glm::vec2 GroundedState::Friction(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// pre
	glm::vec2 force(0.0f, 0.0f);

	// body
	force = -physicsComponent.Velocity;
	force *= physicsComponent.PrecalculatedValues.airDrag;
	force *= physicsComponent.PrecalculatedValues.staticFriction;
	force += physicsComponent.PrecalculatedValues.gravity;

	return force;
}

// --------------------- Standing ---------------------

inline void StandingState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	GroundedState::OnEnter(physicsComponent);

	// texture change
	physicsComponent.UpdateTexture((lookRight_ ? PlayerTextureStore::PlayerTextureID::STAND_R : PlayerTextureStore::PlayerTextureID::STAND_L), GL_TRUE);

	if (DEBUG)
		Logger::Log("standing");
}

inline PlayerState * StandingState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// update tex
	if (input.Down)
		stateNum_ = 66;
	else
		idleTime_ += dt;

	switch (stateNum_)
	{
	case 0:
		if (idleTime_ < 5)
			break;
		physicsComponent.UpdateTexture((PlayerTextureStore::PlayerTextureID::STAND_UR), GL_FALSE);
		++stateNum_;
		break;
	case 1:
		if (idleTime_ < 7)
			break;
		physicsComponent.UpdateTexture((lookRight_ ? PlayerTextureStore::PlayerTextureID::STAND_R : PlayerTextureStore::PlayerTextureID::STAND_L), GL_FALSE);
		++stateNum_;
		break;
	case 2:
		if (idleTime_ < 14)
			break;
		physicsComponent.UpdateTexture((lookRight_ ? PlayerTextureStore::PlayerTextureID::STAND_DL : PlayerTextureStore::PlayerTextureID::STAND_DR), GL_FALSE);
		++stateNum_;
		break;
	case 3:
		if (idleTime_ < 20)
			break;
		physicsComponent.UpdateTexture((lookRight_ ? PlayerTextureStore::PlayerTextureID::STAND_R : PlayerTextureStore::PlayerTextureID::STAND_L), GL_FALSE);
		stateNum_ = 0;
		idleTime_ = 0;
		break;
	case 66:	// look down
		physicsComponent.UpdateTexture((lookRight_ ? PlayerTextureStore::PlayerTextureID::STAND_DR : PlayerTextureStore::PlayerTextureID::STAND_DL), GL_FALSE);
		stateNum_ = 3;
		idleTime_ = 19.7f;
		break;
	default:
		Logger::Log("ERROR::PHYSICS_COMPONENT::StandingState::illegal switch");
		break;
	}

	// transition
	if (physicsComponent.Velocity.x != 0.0f)
		moveTime_ += dt;
	else
		moveTime_ = 0;
	if (moveTime_ > 0.05)
		if (physicsComponent.Velocity.x > 0)
			return new MoveRightState(GL_TRUE);
		else if (physicsComponent.Velocity.x < 0)
			return new MoveLeftState(GL_FALSE);

	// super
	return GroundedState::Update(physicsComponent, dt, input);
}

// --------------------- MoveRight ---------------------

inline void MoveRightState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	GroundedState::OnEnter(physicsComponent);

	startPos_ = physicsComponent.PositionComponentPointer->Position.x;

	if (DEBUG)
		Logger::Log("moveright");
}

inline PlayerState * MoveRightState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// input
	if (input.Left && !input.Right)
	{
		switch ((GLint)(physicsComponent.Velocity.x) / 8)
		{
			// terminal velocity on ground is around 11.
		case 0:	// 0-8
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::LAND_L_33);
			break;
		case 1:	// 9-16
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::LAND_L_66);
			break;
		default:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::LAND_L_100);
			break;
		}
		//Logger::Log("Velocity: " + std::to_string(physicsComponent.Velocity.x));
	}
	else
	{
		// update tex
		switch ((GLint)((physicsComponent.PositionComponentPointer->Position.x - startPos_) * 2) % 4)
		{
		case 0:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_R_0);
			break;
		case 1:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_R_1);
			break;
		case 2:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_R_2);
			break;
		case 3:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_R_3);
			break;
		default:
			Logger::Log("ERROR::PHYSICS_COMPONENT::MoveRightState::illegal switch");
			break;
		}
	}

	// transition
	if (physicsComponent.Velocity.x > 0)
		// super
		return GroundedState::Update(physicsComponent, dt, input);
	else
		return new StandingState(lookRight_);
}

// --------------------- MoveLeft ---------------------

inline void MoveLeftState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	GroundedState::OnEnter(physicsComponent);

	startPos_ = physicsComponent.PositionComponentPointer->Position.x;

	if (DEBUG)
		Logger::Log("moveleft");
}

inline PlayerState * MoveLeftState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// input
	if (input.Right && !input.Left)
	{
		switch ((GLint)(physicsComponent.Velocity.x) / -8)
		{
			// terminal velocity on ground is around 11.
		case 0:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::LAND_R_33);
			break;
		case 1:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::LAND_R_66);
			break;
		default:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::LAND_R_100);
			break;
		}
		//Logger::Log("Velocity: " + std::to_string(physicsComponent.Velocity.x));
	}
	else
	{
		// update tex
		switch ((GLint)((startPos_ - physicsComponent.PositionComponentPointer->Position.x) * 2) % 4)
		{
		case 0:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_L_0);
			break;
		case 1:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_L_1);
			break;
		case 2:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_L_2);
			break;
		case 3:
			physicsComponent.UpdateTexture(PlayerTextureStore::PlayerTextureID::MOVE_L_3);
			break;
		default:
			Logger::Log("ERROR::PHYSICS_COMPONENT::MoveLeftState::illegal switch");
			break;
		}
	}

	// transition
	if (physicsComponent.Velocity.x < 0)
		// super
		return GroundedState::Update(physicsComponent, dt, input);
	else
		return new StandingState(lookRight_);
}

// --------------------- Landing ---------------------

void LandingState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	GroundedState::OnEnter(physicsComponent);

	this->SubID = StateSubID::LANDING;

	// hitbox compensation
	physicsComponent.PositionComponentPointer->Position.y += physicsComponent.GetHitboxMin().y;

	// check the intensity of the landing
	GLuint len = (GLuint)(-physicsComponent.Velocity.y);
	if (len >= 16)
		time_ = 0.3f;
	else if (len >= 9)
		time_ = 0.16f;
	else if (len >= 5)
		time_ = 0.08f;
	else
		time_ = 0.03f;

	GLint xlen = (GLint)(physicsComponent.Velocity.x);
	if (xlen > 5 || xlen < -5)
		strongSideways_ = GL_TRUE;
	else
		strongSideways_ = GL_FALSE;

	//Logger::Log("y v: " + std::to_string(physicsComponent.Velocity.y));
	//// 23 is max on testlevel, 16 is strong, 10 is regular jump, 11 is 3 block fall, 4 is min measured
	//Logger::Log("x v: " + std::to_string(physicsComponent.Velocity.x));
	//// 19 is max, 5 is light

	if (DEBUG)
		Logger::Log("landing");
}

PlayerState * LandingState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	time_ -= dt;

	// update tex
	if (strongSideways_)
		if (time_ > 0.16f)
			physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::LAND_L_100 : PlayerTextureStore::PlayerTextureID::LAND_R_100);
		else if (time_ > 0.08f)
			physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::LAND_L_66 : PlayerTextureStore::PlayerTextureID::LAND_R_66);
		else if (time_ > 0.0f)
			physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::LAND_L_33 : PlayerTextureStore::PlayerTextureID::LAND_R_33);
		else
			// transition
			return new StandingState(lookRight_);
	else
		if (time_ > 0.16f)
			physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::CROUCH_R_100 : PlayerTextureStore::PlayerTextureID::CROUCH_L_100);
		else if (time_ > 0.08f)
			physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::CROUCH_R_66 : PlayerTextureStore::PlayerTextureID::CROUCH_L_66);
		else if (time_ > 0.0f)
			physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::CROUCH_R_33 : PlayerTextureStore::PlayerTextureID::CROUCH_L_33);
		else
			// transition
			return new StandingState(lookRight_);

	// super (with changed Force/Friction)
	return GroundedState::Update(physicsComponent, dt, input);
}

glm::vec2 LandingState::Force(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	return glm::vec2();
}

glm::vec2 LandingState::Friction(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// pre
	glm::vec2 force(0.0f, 0.0f);

	// body
	force = -physicsComponent.Velocity;
	force *= physicsComponent.PrecalculatedValues.airDrag;
	force *= physicsComponent.PrecalculatedValues.staticFriction * physicsComponent.LandingFrictionMultiplier;
	force += physicsComponent.PrecalculatedValues.gravity;

	return force;
}

// --------------------- Crouching ---------------------

void CrouchingState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	GroundedState::OnEnter(physicsComponent);

	this->SubID = StateSubID::CROUCHING;

	halfPowerTime_ = physicsComponent.HalfPowerTime;
	fullPowerTime_ = physicsComponent.FullPowerTime;

	if (DEBUG)
		Logger::Log("crouching");
}

PlayerState * CrouchingState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	time_ += dt;

	// update tex
	if (time_ > fullPowerTime_)
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::CROUCH_R_100 : PlayerTextureStore::PlayerTextureID::CROUCH_L_100);
	else if (time_ > halfPowerTime_)
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::CROUCH_R_66 : PlayerTextureStore::PlayerTextureID::CROUCH_L_66);
	else
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::CROUCH_R_33 : PlayerTextureStore::PlayerTextureID::CROUCH_L_33);

	// transition
	if (input.Up)
	{
		if (time_ > fullPowerTime_)
			return new JumpingState(lookRight_, physicsComponent.JumpForceBurstValue + 2000, physicsComponent.JumpTime);
		else if (time_ > halfPowerTime_)
			return new JumpingState(lookRight_, physicsComponent.JumpForceBurstValue + 1000, physicsComponent.JumpTime);
		else
			return new JumpingState(lookRight_);
	}
	else if (input.Crouch)
		// super (with modified Force/Friction)
		return GroundedState::Update(physicsComponent, dt, input);
	else
		return new StandingState(lookRight_);
}

//* copy of LandingState::Force()
glm::vec2 CrouchingState::Force(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	return glm::vec2();
}

//* copy of LandingState::Friction()
glm::vec2 CrouchingState::Friction(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// pre
	glm::vec2 force(0.0f, 0.0f);

	// body
	force = -physicsComponent.Velocity;
	force *= physicsComponent.PrecalculatedValues.airDrag;
	force *= physicsComponent.PrecalculatedValues.staticFriction * physicsComponent.LandingFrictionMultiplier;
	force += physicsComponent.PrecalculatedValues.gravity;

	return force;
}


// ---------------------------------------------------------------------------------------- Player States : AERIAL ----------------------------------------------------------------------------------------

inline void AerialState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	PlayerState::OnEnter(physicsComponent);

	this->ID = StateID::AERIAL;
}

PlayerState * AerialState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// transition
	//if (input.Up && input.Down)
	//	;
	//else if (input.Up)
	//	;
	//else if (input.Down)
	//	; // force dive?

	// super
	return PlayerState::Update(physicsComponent, dt, input);
}

glm::vec2 AerialState::Force(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// pre
	glm::vec2 force(physicsComponent.AirMoveForce, 0.0f);
	glm::vec2 directionVector(0.0f, 0.0f);

	// body
	if (input.Left && input.Right)
		;
	else if (input.Left)
		directionVector.x = -1;
	else if (input.Right)
		directionVector.x = 1;

	force *= directionVector;

	return force;
}

glm::vec2 AerialState::Friction(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// pre
	glm::vec2 force(0.0f, 0.0f);

	// body
	force = -physicsComponent.Velocity;
	force *= physicsComponent.PrecalculatedValues.airDrag;
	force += physicsComponent.PrecalculatedValues.gravity;

	return force;
}

// --------------------- Jumping ---------------------

inline void JumpingState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	AerialState::OnEnter(physicsComponent);

	this->SubID = StateSubID::JUMPING;

	physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::RISE_R_50 : PlayerTextureStore::PlayerTextureID::RISE_L_50);

	if (totalJumpTime_ == 0.0f)
	{
		totalJumpTime_ = physicsComponent.JumpTime;
		jumpForceBurstValue_ = physicsComponent.JumpForceBurstValue;
	}

	forceSeconds_ = jumpForceBurstValue_ * totalJumpTime_;

	if (DEBUG)
		Logger::Log("jumping");
}

inline PlayerState * JumpingState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// transition
	if (input.Up && forceSeconds_ > 0)
		// super (with modified Force())
		return AerialState::Update(physicsComponent, dt, input);
	else
		return new RisingState(lookRight_);
}

glm::vec2 JumpingState::Force(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	jumpTime_ += dt;

	if (jumpTime_ > totalJumpTime_ + 0.1f) {
		Logger::Log("ERROR::PHYSICS_COMPONENT::JUMPING_STATE::max time expired");
	}
	else if (input.Up)
	{
		GLfloat power = jumpForceBurstValue_ * dt;
		forceSeconds_ -= power;
		if (forceSeconds_ > 0)
			return glm::vec2(0.0f, jumpForceBurstValue_);
		else if (forceSeconds_ + power < 0)
			return glm::vec2();
		else
			return glm::vec2(0.0f, ((power + forceSeconds_) / power) * jumpForceBurstValue_);
	}
	else {
		Logger::Log("ERROR::PHYSICS_COMPONENT::JUMPING_STATE::branch not allowed");
	}
	// return on error
	return glm::vec2();
}

// --------------------- Rising ---------------------

inline void RisingState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	AerialState::OnEnter(physicsComponent);

	if (DEBUG)
		Logger::Log("rising");
}

inline PlayerState * RisingState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// update tex
	if (physicsComponent.Velocity.x != 0)
		lookRight_ = physicsComponent.Velocity.x > 0 ? GL_TRUE : GL_FALSE;

	switch ((GLint)(physicsComponent.Velocity.y) / 5)
	{
	case 0:
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::RISE_R_50 : PlayerTextureStore::PlayerTextureID::RISE_L_50);
		break;
	default:
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::RISE_R_100 : PlayerTextureStore::PlayerTextureID::RISE_L_100);
		break;
	}
	//Logger::Log("Velocity: " + std::to_string(physicsComponent.Velocity.y));

	// transition
	if (physicsComponent.prevPos.y > physicsComponent.PositionComponentPointer->Position.y)
		return new FallingState(lookRight_);

	// super
	return AerialState::Update(physicsComponent, dt, input);
}

// --------------------- Falling ---------------------

inline void FallingState::OnEnter(PlayerPhysicsComponent & physicsComponent)
{
	// super
	AerialState::OnEnter(physicsComponent);

	if (DEBUG)
		Logger::Log("falling");
}

inline PlayerState * FallingState::Update(PlayerPhysicsComponent & physicsComponent, GLfloat dt, PlayerInput & input)
{
	// update tex
	if (physicsComponent.Velocity.x != 0)
		lookRight_ = physicsComponent.Velocity.x > 0 ? GL_TRUE : GL_FALSE;

	switch ((GLint)(physicsComponent.Velocity.y) / 5)
	{
	case 0:
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::FALL_R_50 : PlayerTextureStore::PlayerTextureID::FALL_L_50);
		break;
	default:
		physicsComponent.UpdateTexture(lookRight_ ? PlayerTextureStore::PlayerTextureID::FALL_R_100 : PlayerTextureStore::PlayerTextureID::FALL_L_100);
		break;
	}
	//Logger::Log("Velocity: " + std::to_string(physicsComponent.Velocity.y));

	// transition
	if (physicsComponent.prevPos.y < physicsComponent.PositionComponentPointer->Position.y)
		return new RisingState(lookRight_);

	// super
	return AerialState::Update(physicsComponent, dt, input);
}