#pragma once
#include "Texture2D.h"
#include "Shader.h"
#include "UBOBindingWorks.h"

#include <vector>

class InstancedRenderer
{
public:
	// init/fini
	InstancedRenderer() = default;
	~InstancedRenderer() = default;
	void Delete();
	void Init(Shader& shader, UBOBindingWorks& uboBindings, std::vector<glm::vec3>& instanceData, Texture2D& texture, glm::ivec2& texDivisor, glm::vec4& color);
	// update data
	//void UpdateData(std::vector<glm::vec3>& instanceData);
	// render
	void DrawInstanced();

private:
	// Render state
	Shader InShader;
	GLuint VAO = NULL;
	Texture2D* Texture;
	GLuint ObjectsNum;

	GLuint ConstantsUBO;

	// Initializes and configures the quad's buffer and vertex attributes
	void InitRenderData(std::vector<glm::vec3>& instanceData);
	void InitConstantsUBO(glm::vec4 color, glm::ivec2 texDivisor, UBOBindingWorks& uboBindings);
	void BindMVPMatricesUBO(UBOBindingWorks& uboBindings);
};

class DynamicRenderer
{
public:
	// init/fini
	DynamicRenderer() = default;
	~DynamicRenderer() = default;
	void Delete();
	void Init(Shader& shader, UBOBindingWorks& uboBindings, Texture2D& texture, glm::ivec2& texDivisor, glm::vec4& color);
	// update data
	void UpdateData(glm::vec2& position, GLuint texNum);
	void UpdateData(glm::vec4& color, glm::vec2& position, glm::vec2& size, GLuint texNum);
	// render
	void Draw();

private:
	// Render state
	Shader DyShader;
	GLuint VAO = NULL;
	Texture2D* Texture;

	GLuint ConstantsUBO;
	GLuint DynamicsUBO;

	// Initializes and configures the quad's buffer and vertex attributes
	void InitRenderData();
	void InitConstantsUBO(glm::ivec2& texDivisor, UBOBindingWorks& uboBindings);
	void InitDynamicsUBO(glm::vec4& color, glm::vec2& position, glm::vec2& size, GLuint texNum, UBOBindingWorks& uboBindings);
	void BindMVPMatricesUBO(UBOBindingWorks& uboBindings);
};

class ParallaxRenderer
{
public:
	// init/fini
	ParallaxRenderer() = default;
	~ParallaxRenderer() = default;
	void Delete();
	void Init(Shader& shader, UBOBindingWorks& uboBindings, Texture2D& texture);
	// update data
	void UpdateData(glm::vec2& position);
	void UpdateData(glm::vec4& color, glm::vec2& position, glm::vec2& size);
	// render
	void Draw();

private:
	// Render state
	Shader PaShader;
	GLuint VAO = NULL;
	Texture2D* Texture;

	GLuint DynamicsUBO;

	// Initializes and configures the quad's buffer and vertex attributes
	void InitRenderData();
	void InitDynamicsUBO(UBOBindingWorks& uboBindings);
	void BindMVPMatricesUBO(UBOBindingWorks& uboBindings);
};