#pragma once

class UBOBindingWorks
{
public:
	enum UBO
	{
		UBOProjectionData = 0,
	};

	UBOBindingWorks() { current_ubo_ = private_ubo_bindings_start_ - 1; };
	~UBOBindingWorks() {};

	GLuint GetFreeBinding() {
		return ++current_ubo_;
	};

private:
	const GLuint private_ubo_bindings_start_ = 1;
	GLuint current_ubo_;
};