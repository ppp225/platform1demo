#pragma once

// --------------------- Position Component ---------------------

class PositionComponent
{
public:
	PositionComponent(glm::vec2 position, glm::vec2 size);

	glm::vec2 Position, Size;
};