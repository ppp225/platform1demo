#pragma once
#include "PositionComponent.h"
#include "RenderComponent.h"
#include "PhysicsComponent.h"
#include "InputComponent.h"
#include "Camera.h"

#include <vector>

class GameObject
{
public:
	GameObject() {};
	GameObject(PositionComponent* positionComponent, RenderComponent* renderComponent = NULL, PhysicsComponent* physicsComponent = NULL,  InputComponent* inputComponent = NULL);
	~GameObject() {};

	GLulong ID;
	PositionComponent* position_comp_;
	PhysicsComponent* physics_comp_;
	RenderComponent* render_comp_;
	InputComponent* input_comp_;
};

class TheWorld
{
public:
	TheWorld() {};
	~TheWorld() {};

	glm::vec2 Size;

	SimpleCamera Camera;
	std::vector<GameObject> PlayerObjects, StaticObjects, InteractiveStaticObjects, ParallaxObjects;
};

// TODO: make it a static GameLevelLoader
class GameLevel
{
public:
	GameLevel() {};
	~GameLevel() {};

	void Load(const GLchar* file, TheWorld* world);

	glm::vec2 GetPlayerStartingPosition() { return this->PlayerStartingPosition; };

private:
	glm::vec2 PlayerStartingPosition;

	void Init(std::vector<std::vector<GLuint>>& tileData, TheWorld* world);

};