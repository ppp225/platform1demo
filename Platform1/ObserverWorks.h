#pragma once
#include <list>
namespace obw {

	// states

	enum Action
	{
		RELEASE = 0,
		PRESS = 1,
		REPEAT = 2,
	};

	/** MousePositionObserver -----------------------------------------------------------------------------------------------------------------------
	*/
	class MousePositionObserver
	{
	public:
		virtual ~MousePositionObserver() {}
		virtual void OnMousePosition(GLuint xpos, GLuint ypos) = 0;
	};

	class MousePositionSubject
	{
	public:
		void addObserver(MousePositionObserver& observer) { Observers.push_back(&observer); }

		void removeObserver(MousePositionObserver& observer) { Observers.remove(&observer); }

		void Notify(GLuint xpos, GLuint ypos) {
			for (auto o : Observers)
				o->OnMousePosition(xpos, ypos);
		}
	private:
		std::list<MousePositionObserver*> Observers;
	};

	/** MouseButtonObserver -----------------------------------------------------------------------------------------------------------------------
	*/
	class MouseButtonObserver
	{
	public:
		virtual ~MouseButtonObserver() {}
		virtual void OnMouseAction(GLuint button, GLuint action, GLuint mods) = 0;
	};

	class MouseButtonSubject
	{
	public:
		void addObserver(MouseButtonObserver& observer) { Observers.push_back(&observer); }

		void removeObserver(MouseButtonObserver& observer) { Observers.remove(&observer); }

		void Notify(GLuint button, GLuint action, GLuint mods) {
			for (auto o : Observers)
				o->OnMouseAction(button, action, mods);
		}
	private:
		std::list<MouseButtonObserver*> Observers;
	};

	/** KeyInputObserver -----------------------------------------------------------------------------------------------------------------------
	*/
	class KeyInputObserver
	{
	public:
		virtual ~KeyInputObserver() {}
		virtual void OnKeyInput(GLuint key, GLuint scancode, GLuint action, GLuint mods) = 0;
	};

	class KeyInputSubject
	{
	public:
		void addObserver(KeyInputObserver& observer) { Observers.push_back(&observer); }

		void removeObserver(KeyInputObserver& observer) { Observers.remove(&observer); }

		void Notify(GLuint key, GLuint scancode, GLuint action, GLuint mods) {
			for (auto o : Observers)
				o->OnKeyInput(key, scancode, action, mods);
		}
	private:
		std::list<KeyInputObserver*> Observers;
	};

	//template <class ObsT>	//TODO: make a templated observer for other events maybe
	//class Subject {
	//public:
	//	void addObserver(ObsT& observer)
	//	{
	//		Observers.push_back(&observer);
	//	}

	//	void removeObserver(ObsT& observer)
	//	{
	//		Observers.remove(&observer);
	//	}

	//	template <GLint... values>
	//	void Notify(GLint[] values)
	//	{
	//		for (auto o : Observers)
	//		{
	//			o->OnKeyInput(key, scancode, action, mods);
	//		}
	//	}

	//private:
	//	std::list<ObsT*> Observers;

	//};
}