#pragma once
#include <unordered_map>

enum class GameState {
	GAME_ACTIVE,
	GAME_PAUSE,
	GAME_RESTART,
	GAME_MENU,
	GAME_OVER
};

enum class GameAction {
	Player1UP,
	Player1DOWN,
	Player1LEFT,
	Player1RIGHT,
	Player1CROUCH,

	Pause,
	Restart,
	Options,

	Fullscreen,
	CloseApp,
};

enum class AI {
	Player1,
	//Player2,
};

class SettingStore
{
public:
	SettingStore();
	~SettingStore();

	GLint GetKey(GameAction action);
	void ChangeKeyBinding(GameAction action, GLint key);
	void RestoreDefualtKeys();
private:
	std::unordered_map<GameAction, GLint> KeyMapping;
};

