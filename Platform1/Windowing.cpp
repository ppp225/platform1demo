#include "stdafx.h"
#include "Windowing.h"


Windowing::Windowing() : TheGame(WINDOW_WIDTH, WINDOW_HEIGHT)
{
}


Windowing::~Windowing()
{
	glfwTerminate();
}

GLint Windowing::Init()
{
	// GLFW
	if (!glfwInit())
		return 1;

	// window creation
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);


	//glfwWindowHint(GLFW_SAMPLES, 4);
	//glEnable(GL_MULTISAMPLE);


	window_ = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_NAME.c_str(), nullptr, nullptr);
	glfwSetWindowPos(window_, WINDOW_POSITION_X, WINDOW_POSITION_Y);
	glfwMakeContextCurrent(window_);

	// glew
	glewExperimental = GL_TRUE;
	glewInit();
	glGetError(); // Call it once to catch glewInit() bug, all other errors are now from our application.

	// callbacks
	glfwSetWindowUserPointer(window_, this);

	glfwSetKeyCallback(window_, key_callback);
	glfwSetCursorPosCallback(window_, mouse_pos_callback);
	glfwSetMouseButtonCallback(window_, mouse_button_callback);
	//glfwSetScrollCallback(window, scroll_callback);
	glfwSetWindowPosCallback(window_, window_pos_callback);
	glfwSetWindowSizeCallback(window_, window_size_callback);

	glfwSwapInterval(VSYNC_);	//vsync 0 = off. 1 = screenFPS = 60. 2=30. 3=15.

	// OpenGL configuration
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

void Windowing::Run()
{
	// game init
	TheGame.SetSubjects(mouse_position_subject_, mouse_button_subject_, key_input_subject_);
	TheGame.Init();

	// DeltaTime variables
	GLfloat deltaTime = 0.0f;
	GLfloat lastFrame = 0.0f;

	GLfloat upsTime_ = 0;
	GLuint ups_ = 0;

	GLfloat displayTimeGrain = 1000.0f;
	GLuint physicsNum = 0;
	GLfloat physicsTime = 0.0f;
	GLuint renderNum = 0;
	GLfloat renderTime = 0.0f;

	LimitFPS = 250;

	GLfloat renderAccTime = 0.0f;
	GLfloat renderUpsTime_ = 1.0f / LimitFPS;

	while (!glfwWindowShouldClose(window_)) {
		// Delta time
		GLfloat currentFrame = (GLfloat)(glfwGetTime());
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		if (deltaTime > 0.25)
			continue;

		++ups_;
		upsTime_ += deltaTime;

		// Input
		glfwPollEvents();	// must be called from main thread

		// game
		{
			{
				// update
				GLfloat start = (GLfloat)(glfwGetTime());
				GLuint num = TheGame.Update(deltaTime);
				physicsNum += num;
				GLfloat finish = (GLfloat)(glfwGetTime());
				if (num > 0)
					physicsTime += (finish - start);
			}
			// render
			if (LimitFPS)	// != 0
			{
				renderAccTime += deltaTime;
				while (renderAccTime >= renderUpsTime_)
				{
					GLfloat start = (GLfloat)(glfwGetTime());

					++renderNum;
					renderAccTime -= renderUpsTime_;
					TheGame.Render();
					// buffers
					glfwSwapBuffers(window_);

					GLfloat finish = (GLfloat)(glfwGetTime());
					renderTime += (finish - start);
				}
			}
			else
			{
				GLfloat start = (GLfloat)(glfwGetTime());

				++renderNum;
				TheGame.Render();
				// buffers
				glfwSwapBuffers(window_);

				GLfloat finish = (GLfloat)(glfwGetTime());
				renderTime += (finish - start);
			}
		}

		// diag
		if (upsTime_ >= 1.0f)
		{
			physicsTime /= physicsNum;
			renderTime /= renderNum;

			// print data
			std::cout << "\r" << "UPS: " << ups_
				//<< " | AvgFrame: " << (GLint)(1.0f / ups_ * 1000 * displayTimeGrain) / displayTimeGrain << "ms (" << ups_ << ")"
				<< " | AvgPhysics: " << ((GLint)(physicsTime * 1000 * displayTimeGrain)) / displayTimeGrain << "ms (" << physicsNum << ")"
				<< " | AvgRender: " << ((GLint)(renderTime * 1000 * displayTimeGrain)) / displayTimeGrain << "ms (" << renderNum << ")"
				<< "      ";

			physicsTime = renderTime = 0.0f;
			physicsNum = renderNum = 0;

			// FPS limiter check. Turn off if not enough FPS
			if (ups_ < LimitFPS)
				LimitFPS = 0;

			ups_ = 0;
			upsTime_ = 0;
		}
	}
}

void Windowing::UpdateResolution(GLuint width, GLuint height, GLboolean fullscreen)
{
	WINDOW_WIDTH = width;
	WINDOW_HEIGHT = height;
	SCREEN_HEIGHT = height;

	if (fullscreen && !IsFullscreen_)
		MakeFullScreened();
	else if (!fullscreen && IsFullscreen_)
		MakeWindowed();
}

void Windowing::HandleKeys(GLFWwindow * window, GLuint key, GLuint scancode, GLuint action, GLuint mods)
{
	GLint closeAppKey = TheGame.getSS().GetKey(GameAction::CloseApp);
	GLint fullscreenKey = TheGame.getSS().GetKey(GameAction::Fullscreen);

	// close app
	if (key == closeAppKey && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	// fullscreen
	if (key == fullscreenKey && action == GLFW_RELEASE)
		fullscreen_key_processed = GL_FALSE;

	if (key == fullscreenKey && action == GLFW_PRESS && !fullscreen_key_processed) {
		fullscreen_key_processed = GL_TRUE;
		UpdateResolution(WINDOW_WIDTH, WINDOW_HEIGHT, !IsFullscreen_);
	}
}

void Windowing::MakeFullScreened()
{
	// disabling callbacks
	glfwSetWindowSizeCallback(window_, NULL);	// crashes on fullscreen
	glfwSetWindowPosCallback(window_, NULL);	// malfunctions

	// fullscreening
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	glfwSetWindowMonitor(window_, monitor, -1, -1, mode->width, mode->height, mode->refreshRate);
	SCREEN_HEIGHT = mode->height;

	IsFullscreen_ = GL_TRUE;
	// update game
	TheGame.UpdateResolution(mode->width, mode->height);

}

void Windowing::MakeWindowed()
{
	// windowing
	glfwSetWindowMonitor(window_, NULL, WINDOW_POSITION_X, WINDOW_POSITION_Y, WINDOW_WIDTH, WINDOW_HEIGHT, -1);
	SCREEN_HEIGHT = WINDOW_HEIGHT;

	// restoring callbacks
	glfwSetWindowSizeCallback(window_, window_size_callback);
	glfwSetWindowPosCallback(window_, window_pos_callback);

	IsFullscreen_ = GL_FALSE;

	// update game
	TheGame.UpdateResolution(WINDOW_WIDTH, WINDOW_HEIGHT);
}

//GLint lasttimems = 0;
void Windowing::key_callback(GLFWwindow * window, int key, int scancode, int action, int mods)
{
	Windowing * win = reinterpret_cast<Windowing *>(glfwGetWindowUserPointer(window));
	win->HandleKeys(window, key, scancode, action, mods);
	win->key_input_subject_.Notify(key, scancode, action, mods);
	//Logger::Log(std::to_string(key) + " " + std::to_string(scancode) + " " + std::to_string(action) + " " + std::to_string(mods));

	// keyboard input lag test
	//Logger::Log(std::to_string(action) + " " + std::to_string( (GLint)(glfwGetTime() * 1000) - lasttimems));
	//lasttimems = (GLint)(glfwGetTime() * 1000);
}

void Windowing::mouse_pos_callback(GLFWwindow * window, double xpos, double ypos)
{
	Windowing * win = reinterpret_cast<Windowing *>(glfwGetWindowUserPointer(window));
	ypos = win->SCREEN_HEIGHT - ypos - 1.0;
	win->mouse_position_subject_.Notify((GLint)xpos, (GLint)ypos);
	//Logger::Log(std::to_string(xpos) + " " + std::to_string(ypos));
}

void Windowing::mouse_button_callback(GLFWwindow * window, int button, int action, int mods)
{
	Windowing * win = reinterpret_cast<Windowing *>(glfwGetWindowUserPointer(window));
	win->mouse_button_subject_.Notify(button, action, mods);
	//Logger::Log(std::to_string(button) + " " + std::to_string(action));
}

void Windowing::window_size_callback(GLFWwindow * window, int width, int height)
{
	Windowing * win = reinterpret_cast<Windowing *>(glfwGetWindowUserPointer(window));
	win->WINDOW_WIDTH = width;
	win->WINDOW_HEIGHT = height;
	win->SCREEN_HEIGHT = height;
	glfwSetWindowMonitor(window, NULL, win->WINDOW_POSITION_X, win->WINDOW_POSITION_Y, win->WINDOW_WIDTH, win->WINDOW_HEIGHT, -1);

	win->TheGame.UpdateResolution(width, height);
}

void Windowing::window_pos_callback(GLFWwindow * window, int xpos, int ypos)
{
	if (ypos <= 0)
		return;

	Windowing * win = reinterpret_cast<Windowing *>(glfwGetWindowUserPointer(window));
	win->WINDOW_POSITION_X = xpos;
	win->WINDOW_POSITION_Y = ypos;
}
