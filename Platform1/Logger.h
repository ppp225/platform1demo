#pragma once
#include <string>
#include <iostream>

class Logger
{
public:
	static void Log(std::string type, std::string loc, std::string msg)
	{
		std::cout << "Log: " << type << "::" << loc << ": " << msg << std::endl;
	};
	static void Log(std::string msg)
	{
		std::cout << "Log: " << msg << std::endl;
	};
private:
	Logger() {};
};

