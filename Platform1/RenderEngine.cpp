#include "stdafx.h"
#include "RenderEngine.h"

// --------------------- xxxxxx ---------------------

RenderEngine::~RenderEngine()
{
	for (auto& var : this->PlayerObjectContainers)
		var.Delete();
	for (auto& var : this->RenderTileContainers)
		var.Delete();
	for (auto& var : this->ParallaxContainers)
		var.Delete();
}

void RenderEngine::Init(TheWorld & world, UBOBindingWorks& uboBindings)
{
	this->WorldPointer = &world;

	// static objects
	Shader stShader = Shader::LoadShader("Data/Shaders/instancedTiles.vert", "Data/Shaders/instancedTiles.frag", nullptr);

	struct Data {
		Data(GLuint type, GLuint texID, glm::vec4 color) : Type(type), TexID(texID), Color(color) {};
		GLuint Type;
		GLuint TexID;
		glm::vec4 Color;
	};

	std::vector<Data> types;

	for (auto& var : world.StaticObjects)
	{
		GLboolean cont = GL_FALSE;
		Data d(var.render_comp_->Type, var.render_comp_->TextureID, var.render_comp_->Color);
		for (auto& type : types)
		{
			if (var.render_comp_->Type == type.Type && var.render_comp_->TextureID == type.TexID && var.render_comp_->Color == type.Color) 
			{
				cont = GL_TRUE;
				break;
			}
		}
		if (cont)
			continue;

		types.push_back(d);
	}
	
	RenderTileContainers.reserve(types.size());
	for (GLuint i = 0; i < types.size(); ++i)
	{
		auto& type = *std::next(types.begin(), i);

		this->RenderTileContainers.push_back(SimpleInstancedTileContainer());
		SimpleInstancedTileContainer& sitc = this->RenderTileContainers[(RenderTileContainers.size() - 1)];

		for (auto& var : world.StaticObjects)
		{
			if (var.render_comp_->Type == type.Type && var.render_comp_->TextureID == type.TexID && var.render_comp_->Color == type.Color)
			{
				glm::vec3 pos = glm::vec3(var.position_comp_->Position.xy, (GLfloat)var.render_comp_->TextureNum);
				sitc.ObjectPositions.push_back(pos);
			}
		}

		sitc.InitRenderer(stShader, uboBindings, TextureStore.GetTexture(type.TexID), TextureStore.GetTextureDivisor(type.TexID), type.Color);
	}

	// dynamic objects
	Shader dyShader = Shader::LoadShader("Data/Shaders/dynamicRenderer.vert", "Data/Shaders/dynamicRenderer.frag", nullptr);

	for (auto& var : world.PlayerObjects)
	{
		this->PlayerObjectContainers.push_back(DynamicObjectContainer());
		DynamicObjectContainer& doc = this->PlayerObjectContainers[(PlayerObjectContainers.size() - 1)];

		doc.InitRenderer(dyShader, uboBindings, TextureStore.GetTexture(var.render_comp_->TextureID), TextureStore.GetTextureDivisor(var.render_comp_->TextureID), var.render_comp_->Color);
		doc.UpdateData(var.render_comp_->Color, var.position_comp_->Position, var.position_comp_->Size, var.render_comp_->TextureNum);
	}

	// parallax
	ParallaxContainers.reserve(world.ParallaxObjects.size());
	for (auto& var : world.ParallaxObjects)
	{
		Shader paShader = Shader::LoadShader("Data/Shaders/parallax.vert", "Data/Shaders/parallax.frag", nullptr);

		this->ParallaxContainers.push_back(ParallaxContainer());
		ParallaxContainer& pc = this->ParallaxContainers[ParallaxContainers.size() - 1];

		pc.InitRenderer(paShader, uboBindings, TextureStore.GetTexture(var.render_comp_->TextureID));
		pc.UpdateData(var.render_comp_->Color, var.position_comp_->Position, var.position_comp_->Size);
	}
}

void RenderEngine::UpdateGLBufferedAssetResolution()
{
	GLuint i = 0;
	for (auto& var : this->WorldPointer->ParallaxObjects)
	{
		ParallaxContainer& pc = this->ParallaxContainers[i];
		pc.UpdateData(var.render_comp_->Color, var.position_comp_->Position, var.position_comp_->Size);
		++i;
	}
}

void RenderEngine::Render()
{
	GLuint i = 0;
	for (auto& var : this->ParallaxContainers)
	{
		auto& par = this->WorldPointer->ParallaxObjects[i];
		var.UpdateData(par.position_comp_->Position);
		var.Render();
		++i;
	}

	for (auto& var : this->RenderTileContainers)
		var.Render();

	// player
	i = 0;
	for (auto& var : this->WorldPointer->PlayerObjects) {
		PlayerObjectContainers[i].UpdateData(var.position_comp_->Position, var.render_comp_->TextureNum);
		PlayerObjectContainers[i].Render();
		++i;
	}
}