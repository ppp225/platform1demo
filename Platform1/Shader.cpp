#include "stdafx.h"
#include "Shader.h"

#include <sstream>
#include <fstream>

void Shader::Delete() {
	glUseProgram(0);
	if (this->ID != NULL)
		glDeleteProgram(this->ID);
}

Shader& Shader::Use()
{
	glUseProgram(this->ID);
	return *this;
}

void Shader::Compile(const GLchar* vertexSource, const GLchar* fragmentSource, const GLchar* geometrySource)
{
	GLuint sVertex, sFragment, gShader;
	// vertex shader
	sVertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(sVertex, 1, &vertexSource, NULL);
	glCompileShader(sVertex);
	CheckCompileErrors(sVertex, "VERTEX");
	// fragment shader
	sFragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(sFragment, 1, &fragmentSource, NULL);
	glCompileShader(sFragment);
	CheckCompileErrors(sFragment, "FRAGMENT");
	// geometry shader (optional)
	if (geometrySource != nullptr)
	{
		gShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(gShader, 1, &geometrySource, NULL);
		glCompileShader(gShader);
		CheckCompileErrors(gShader, "GEOMETRY");
	}
	// shader program
	this->ID = glCreateProgram();
	glAttachShader(this->ID, sVertex);
	glAttachShader(this->ID, sFragment);
	if (geometrySource != nullptr)
		glAttachShader(this->ID, gShader);
	glLinkProgram(this->ID);
	CheckCompileErrors(this->ID, "PROGRAM");
	// cleanup. shaders are linked into program, so no longer necessery
	glDeleteShader(sVertex);
	glDeleteShader(sFragment);
	if (geometrySource != nullptr)
		glDeleteShader(gShader);
}

void Shader::SetUniformBlockBinding(const GLchar * name, const GLuint binding)
{
	glUniformBlockBinding(this->ID, glGetUniformBlockIndex(this->ID, name), binding);
}


void Shader::CheckCompileErrors(GLuint object, std::string type)
{
	GLint success;
	GLchar infoLog[1024];
	if (type != "PROGRAM")
	{
		glGetShaderiv(object, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(object, 1024, NULL, infoLog);
			Logger::Log("| ERROR::SHADER: Compile-time error: Type: " + type + "\n"
				+ infoLog + "\n -- --------------------------------------------------- -- ");
		}
	}
	else
	{
		glGetProgramiv(object, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(object, 1024, NULL, infoLog);
			Logger::Log("| ERROR::Shader: Link-time error: Type: " + type + "\n"
				+ infoLog + "\n -- --------------------------------------------------- -- ");
		}
	}
}

Shader& Shader::LoadShader(const GLchar *vShaderFile, const GLchar *fShaderFile, const GLchar *gShaderFile)
{
	return LoadShaderFromFile(vShaderFile, fShaderFile, gShaderFile);
}

Shader& Shader::LoadShaderFromFile(const GLchar *vShaderFile, const GLchar *fShaderFile, const GLchar *gShaderFile)
{
		// 1. retrieve the vertex/fragment source code from filePath
		std::string vertexCode;
		std::string fragmentCode;
		std::string geometryCode;
		std::ifstream vShaderFileStream;
		std::ifstream fShaderFileStream;
		std::ifstream gShaderFileStream;
		// ensures ifstream objects can throw exceptions:
		vShaderFileStream.exceptions(std::ifstream::badbit);
		fShaderFileStream.exceptions(std::ifstream::badbit);
		gShaderFileStream.exceptions(std::ifstream::badbit);
		try
		{
			// open files
			vShaderFileStream.open(vShaderFile);
			fShaderFileStream.open(fShaderFile);
			std::stringstream vShaderStream, fShaderStream;
			// read file's buffer contents into streams
			vShaderStream << vShaderFileStream.rdbuf();
			fShaderStream << fShaderFileStream.rdbuf();
			// close file handlers
			vShaderFileStream.close();
			fShaderFileStream.close();
			// convert stream into string
			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();
			if (gShaderFile != nullptr)
			{
				gShaderFileStream.open(gShaderFile);
				std::stringstream gShaderStream;
				gShaderStream << gShaderFileStream.rdbuf();
				gShaderFileStream.close();
				geometryCode = gShaderStream.str();
			}
		}
		catch (std::ifstream::failure e)
		{
			Logger::Log("ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ");
		}
	const GLchar *vShaderCode = vertexCode.c_str();
	const GLchar *fShaderCode = fragmentCode.c_str();
	const GLchar *gShaderCode = geometryCode.c_str();
	// 2. create shader object from source code
	Shader shader;
	shader.Compile(vShaderCode, fShaderCode, gShaderFile != nullptr ? gShaderCode : nullptr);
	return shader;
}
