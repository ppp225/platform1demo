#include "stdafx.h"
#include "GameEntities.h"
#include "TileTools.h"

// ---------------------------------------------------------------------------------------- Object ----------------------------------------------------------------------------------------

GameObject::GameObject(PositionComponent * positionComponent, RenderComponent * renderComponent, PhysicsComponent * physicsComponent, InputComponent * inputComponent) :
	position_comp_(positionComponent), render_comp_(renderComponent), physics_comp_(physicsComponent), input_comp_(inputComponent)
{
}

// ---------------------------------------------------------------------------------------- Level ----------------------------------------------------------------------------------------

#include <fstream>
#include <sstream>

void GameLevel::Load(const GLchar * file, TheWorld * world)
{
	// load from file
	GLuint tileCode;
	GameLevel level;

	std::string line;
	std::ifstream fstream(file);

	std::vector<std::vector<GLuint>> tileData;
	if (fstream)
	{
		while (std::getline(fstream, line)) // Read each line from level file
		{
			std::istringstream sstream(line);
			std::vector<GLuint> row;
			while (sstream >> tileCode) // Read each word seperated by spaces
				row.push_back(tileCode);
			tileData.push_back(row);
		}
		if (tileData.size() > 0)
		{
			std::vector<std::vector<GLuint>> v2(tileData.rbegin(), tileData.rend()); v2.swap(tileData);
			this->Init(tileData, world);
		}
	}
}

void GameLevel::Init(std::vector<std::vector<GLuint>>& tileData, TheWorld * world)
{
	glm::vec4 color(1.0f, 1.0f, 1.0f, 1.0f);

	// Calculate dimensions
	GLuint height = tileData.size();
	GLuint width = tileData[0].size(); // Note we can index vector at [0] since this function is only called if height > 0

	world->Size = glm::vec2(width, height);
	world->Camera.Min = glm::vec2(0, 0);

	// Initialize level tiles based on tileData

	// tiles
	const GLuint tileTypes = 1;
	for (GLuint t = 1; t < (tileTypes + 1); ++t)
	{
		for (GLuint y = 0; y < height; ++y)
		{
			for (GLuint x = 0; x < width; ++x)
			{
				// Check block type from level data (2D level array)
				if (tileData[y][x] == t) // Solid
				{
					glm::vec2 size(1, 1);
					PositionComponent* pc = new PositionComponent(glm::vec2(x, y), size);
					RenderComponent* rc = new RenderComponent(*pc, RenderComponent::RenderingType::STATIC, 1, TileTools::GetTileType(x, y, tileData), color);
					PhysicsComponent* pxc = new StaticPhysicsComponent(*pc);

					world->StaticObjects.push_back(GameObject(pc, rc, pxc));
				}
				else if (tileData[y][x] == 999)
					PlayerStartingPosition = glm::vec2(x, y);

			}
		}
	}

	// other things than tiles
	for (GLuint y = 0; y < height; ++y)
	{
		for (GLuint x = 0; x < width; ++x)
		{
			if (tileData[y][x] == 999)
				PlayerStartingPosition = glm::vec2(x, y);
		}
	}

	// parallax TODO: load from file
	for (GLuint i = 1002; i >= 1000; --i)
	{
		glm::vec4 color(0.8f, 0.8f, 1.0f, 1.0f);
		GLfloat depth = 1.0f;
		switch (i)
		{
		case 1000:
			depth = 0.2f;
			color *= 1 - depth;
			break;
		case 1001:
			depth = 0.5f;
			color *= 1 - depth;
			break;
		case 1002:
			depth = 0.8f;
			color *= 1 - depth;
			break;
		default:
			Logger::Log("ERROR::GAME_ENTITIES::LEVEL::INIT::undefined switch");
			break;
		}
		color.a = 1.0f;
		//color.r = 0.5f;

		PositionComponent* pc = new PositionComponent(glm::vec2(0, 0), glm::vec2(30, height));
		PhysicsComponent* pxc = new ParallaxPhysicsComponent(*pc, depth);
		RenderComponent* rc = new RenderComponent(*pc, RenderComponent::RenderingType::PARALLAX, i, 0, color);
		world->ParallaxObjects.push_back(GameObject(pc, rc, pxc));
	}
}