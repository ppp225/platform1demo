#pragma once
#include "GameMain.h"
#include "ObserverWorks.h"

class Windowing
{
public:
	Windowing();
	~Windowing();
	GLint Init();
	void Run();

	void UpdateResolution(GLuint width, GLuint height, GLboolean fullscreen);

private:
	// window
	GLFWwindow* window_;
	const std::string WINDOW_NAME = "Platform1";
	GLuint WINDOW_POSITION_X = 550;
	GLuint WINDOW_POSITION_Y = 50;
	GLuint WINDOW_WIDTH = 800;
	GLuint WINDOW_HEIGHT = 600;
	GLuint SCREEN_HEIGHT = WINDOW_HEIGHT;		//* needed to reverse mouse position in fullscreen
	GLboolean IsFullscreen_ = GL_FALSE;
	GLboolean fullscreen_key_processed = GL_FALSE;

	// temp values TODO: read from settings
	GLuint VSYNC_ = 0;	// 0 = off. 1 = on.
	GLuint LimitFPS = 0;	// turn off VSYNC

	// game
	GameMain TheGame;

	// subjects
	obw::MousePositionSubject mouse_position_subject_;
	obw::MouseButtonSubject mouse_button_subject_;
	obw::KeyInputSubject key_input_subject_;

	// helper functions
	void HandleKeys(GLFWwindow * window, GLuint key, GLuint scancode, GLuint action, GLuint mods);
	void MakeFullScreened();
	void MakeWindowed();

	// GLFW functions
	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void mouse_pos_callback(GLFWwindow* window, double xpos, double ypos);
	static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
	static void window_size_callback(GLFWwindow* window, int width, int height);
	static void window_pos_callback(GLFWwindow* window, int xpos, int ypos);
};

