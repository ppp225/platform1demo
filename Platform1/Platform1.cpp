// Platform1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Windowing.h"

#include <set>

int main()
{
	Windowing Win;
	if (Win.Init() != 0)
	{
		return 1;
	}
	Win.Run();

    return 0;
}