#include "stdafx.h"
#include "SettingExecutor.h"


SettingExecutor::SettingExecutor(SettingStore & settingStore, GameState& gameState) : SettingStorePointer(&settingStore), GameStatePointer(&gameState)
{
}

void SettingExecutor::OnKeyInput(GLuint key, GLuint scancode, GLuint action, GLuint mods)
{
	if (action == obw::Action::PRESS)
	{
		if (key == SettingStorePointer->GetKey(GameAction::Pause))
			if (*this->GameStatePointer == GameState::GAME_ACTIVE)
				*this->GameStatePointer = GameState::GAME_PAUSE;
			else if (*this->GameStatePointer == GameState::GAME_PAUSE)
				*this->GameStatePointer = GameState::GAME_ACTIVE;

		if (key == SettingStorePointer->GetKey(GameAction::Restart))
			*this->GameStatePointer = GameState::GAME_RESTART;

		if (key == SettingStorePointer->GetKey(GameAction::Options))
			;
	}
	else if (action == obw::Action::RELEASE)
	{
		;
	}

}