#pragma once
#include "ObserverWorks.h"
#include "SettingStore.h"

class SettingExecutor : public obw::KeyInputObserver
{
public:
	SettingExecutor(SettingStore& settingStore, GameState& gameState);
	~SettingExecutor() = default;

	virtual void OnKeyInput(GLuint key, GLuint scancode, GLuint action, GLuint mods);

private:
	SettingStore* SettingStorePointer;
	GameState* GameStatePointer;
};

