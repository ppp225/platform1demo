#include "stdafx.h"
#include "PhysicsEngine.h"

PhysicsEngine::PhysicsEngine()
{
}

PhysicsEngine::~PhysicsEngine()
{
}

void PhysicsEngine::Init(TheWorld & world)
{
	WorldPointer_ = &world;
}

GLuint PhysicsEngine::Update(GLfloat dt)
{
	GLuint updates = 0;

	// limit physics updates
	accTime_ += dt;
	while (accTime_ >= upsTime_)
	{
		++updates;
		accTime_ -= upsTime_;

		// check collisions between objects: dynamic-static -> resolve -> dynamic-dynamic -> resolve -> dynamic-interactive -> resolve

		for (auto& pla : WorldPointer_->PlayerObjects)
		{
			auto* ppxc = reinterpret_cast<PlayerPhysicsComponent*>(pla.physics_comp_);
			ppxc->Update(upsTime_);
		}

		DoCollisions();
		UpdateCamera();
		UpdateParallax();
	}
	return updates;
}

// ---------------------------------------------------------------------------------------- Collisions ----------------------------------------------------------------------------------------

void PhysicsEngine::DoCollisions()
{
	// player-static collisions
	for (auto& pla : WorldPointer_->PlayerObjects)
	{
		//GLboolean collided = GL_FALSE;
		GLboolean Xcollided = GL_FALSE;
		GLboolean Ycollided = GL_FALSE;
		auto* ppxc = reinterpret_cast<PlayerPhysicsComponent*>(pla.physics_comp_);

		glm::vec2 min1 = (ppxc->GetHitboxMin() + pla.position_comp_->Position);
		glm::vec2 max1 = (ppxc->GetHitboxMax() + pla.position_comp_->Position);
		for (auto& sta : WorldPointer_->StaticObjects)
		{
			glm::vec2 min2 = (sta.physics_comp_->GetHitboxMin() + sta.position_comp_->Position);
			glm::vec2 max2 = (sta.physics_comp_->GetHitboxMax() + sta.position_comp_->Position);
			if (DetectCollision(min1, max1, min2, max2))
			{
				//collided = GL_TRUE;

				// un-collide
				glm::vec2 trans = MinimumTransition(min1, max1, min2, max2);
				pla.position_comp_->Position += trans;

				// update object state
				// ground collision, update friction
				if (trans.y > 0) {
					auto* spxc = reinterpret_cast<StaticPhysicsComponent*>(sta.physics_comp_);
					ppxc->SetFrictionCoefficient(spxc->FrictionCoefficient);

					ppxc->OnPhysics(GL_TRUE, GL_FALSE);
				}

				if (trans.x)
					Xcollided = GL_TRUE;
				else if (trans.y)
					Ycollided = GL_TRUE;

				// update data for future collision checks
				min1 = (ppxc->GetHitboxMin() + pla.position_comp_->Position);
				max1 = (ppxc->GetHitboxMax() + pla.position_comp_->Position);
			}
		}

		// update object state
		if (Ycollided)
			ppxc->Velocity.y = 0.0f;
		else if (Xcollided)
			ppxc->Velocity.x = 0.0f;
		//else // no collision
		//	;

		//if (!collided)
		//	ppxc->OnPhysics(GL_FALSE, GL_TRUE); // TODO: this is now GroundedState::Update; check if it works well
	}
}

GLboolean PhysicsEngine::DetectCollision(glm::vec2 & min1, glm::vec2 & max1, glm::vec2 & min2, glm::vec2 & max2)
{
	if (max1.x >= min2.x && max2.x >= min1.x)	// x axis collision
		if (max1.y >= min2.y && max2.y >= min1.y)	// y axis collision
			return GL_TRUE;
	return GL_FALSE;
}

glm::vec2 PhysicsEngine::MinimumTransition(glm::vec2 & min1, glm::vec2 & max1, glm::vec2 & min2, glm::vec2 & max2)
{
	glm::vec2 pen;

	GLfloat left = (min2.x - max1.x);
	GLfloat right = (max2.x - min1.x);
	GLfloat top = (min2.y - max1.y);
	GLfloat bottom = (max2.y - min1.y);
	
	if (DEBUG) {
		// no intersection
		if (left > 0 || right < 0) Logger::Log("ERROR::PHYSIX_ENGINE::no intersection");
		if (top > 0 || bottom < 0) Logger::Log("ERROR::PHYSIX_ENGINE::no intersection");
	}
	// intersection. work out the mtd on both x and y axes.
	if (std::abs(left) < right)
		pen.x = left;
	else
		pen.x = right;

	if (std::abs(top) < bottom)
		pen.y = top;
	else
		pen.y = bottom;

	// 0 the axis with the largest mtd value.
	if (std::abs(pen.x) < std::abs(pen.y))
		pen.y = 0;
	else
		pen.x = 0;

	return pen;
}

// ---------------------------------------------------------------------------------------- Misc ----------------------------------------------------------------------------------------

void PhysicsEngine::UpdateCamera()
{
	glm::vec2 camPos = this->WorldPointer_->PlayerObjects[0].position_comp_->Position;
	WorldPointer_->Camera.UpdatePosition(camPos);
}

void PhysicsEngine::UpdateParallax()
{
	auto& cam = WorldPointer_->Camera;
	for (auto& par : WorldPointer_->ParallaxObjects) 
	{
		auto* ppxc = reinterpret_cast<ParallaxPhysicsComponent*>(par.physics_comp_);
		ppxc->UpdatePosition(cam.Position.xy);
	}
}
