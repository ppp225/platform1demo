#include "stdafx.h"
#include "GameMain.h"

#include <glm/gtc/matrix_transform.hpp>


GameMain::GameMain(GLuint width, GLuint height) : Width(width), Height(height), Settings(), KeyHandler(this->Settings, this->State), Physics(), Renderer(), World()
{
	this->Projection = glm::ortho(0.0f, (GLfloat)(this->Width), 0.0f, (GLfloat)(this->Height), -1.0f, 1.0f);

	this->Scale = (GLfloat)this->Height / GAME_HEIGHT;

	this->Physics.Init(this->World);

	this->State = GameState::GAME_ACTIVE;
}


GameMain::~GameMain()
{
}

void GameMain::Init()
{
	// check pre init conditions
	if (KeyInputSubjectPointer == nullptr)
		Logger::Log("ERROR::INPUT_SUBJECTS_NOT_INITIALIZED::USE_SET_SUBJECTS_()_FIRST");

	// this is a GL function. settings, camera, scale etc. are initialized in constructor

	// UBO
	GLfloat scaleFactor = this->Scale * this->World.Camera.Zoom * UNIT_SIZE;
	GLInitProjectionDataUBO(this->Projection, this->World.Camera.GetViewMatrix(scaleFactor), scaleFactor);

	// level
	this->Level.Load("Data/Levels/Level1.lvl", &this->World);

	// camera
	GLfloat worldWidthOnScreen = (GLfloat)this->Width / (UNIT_SIZE * this->Scale);
	this->World.Camera.RegularViewCorrection = glm::vec2(-worldWidthOnScreen / 2.0f, -3);
	this->World.Camera.Max = this->World.Size - glm::vec2(worldWidthOnScreen, 16);

	// parallax

	// depth = 0.0f -> world.width
	// 1.0 -> cam.width (worldWidthOnScreen)
	for (auto& var : this->World.ParallaxObjects)
	{
		auto* ppxc = reinterpret_cast<ParallaxPhysicsComponent*>(var.physics_comp_);
		var.position_comp_->Size.x = this->World.Size.x * (1 - ppxc->Depth) + worldWidthOnScreen * (ppxc->Depth);
	}

	// player
	GameObject Player;

	glm::vec2 pos = this->Level.GetPlayerStartingPosition();
	glm::vec2 size(1, 2);

	glm::vec4 color(1.0f, 1.0f, 1.0f, 1.0f);

	PositionComponent* pc = new PositionComponent(pos, size);
	RenderComponent* rc = new RenderComponent(*pc, RenderComponent::RenderingType::DYNAMIC, 999, 0, color);
	PlayerInputComponent* ic = new PlayerInputComponent(this->Settings);
	PlayerPhysicsComponent* pxc = new PlayerPhysicsComponent(*pc, *ic, *rc);
	this->KeyInputSubjectPointer->addObserver(*ic);
	Player = GameObject(pc, rc, pxc, ic);

	this->World.PlayerObjects.push_back(Player);

	// renderer
	this->Renderer.Init(this->World, this->UBOBindings);

	// other
	this->KeyInputSubjectPointer->addObserver(KeyHandler);

	;
	;;
}

void GameMain::SetSubjects(obw::MousePositionSubject & mousePositionSubjectPointer, obw::MouseButtonSubject & mouseButtonSubjectPointer, obw::KeyInputSubject & keyInputSubjectPointer)
{
	this->MousePositionSubjectPointer = &mousePositionSubjectPointer;
	this->MouseButtonSubjectPointer = &mouseButtonSubjectPointer;
	this->KeyInputSubjectPointer = &keyInputSubjectPointer;
}

void GameMain::UpdateResolution(GLuint width, GLuint height)
{
	updateResolution_ = GL_TRUE;
	this->newWidth = width;
	this->newHeight = height;
}

// -------------------------------------------------------------------------- Game Loop ----------------------------------------------

GLuint GameMain::Update(GLfloat dt)
{
	if (this->State == GameState::GAME_RESTART)
		OnRestart();

	if (this->State == GameState::GAME_ACTIVE)
		return this->Physics.Update(dt);

	return 0u;
}

void GameMain::Render()
{
	// background
	glClearColor(0.1f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// updating gl states
	if (updateResolution_)
		GLUpdateResolution();
	if (updateCameraPosition_) {
		GLfloat scaleFactor = this->Scale * this->World.Camera.Zoom * UNIT_SIZE;
		GLUpdateProjectionDataUBO(this->World.Camera.GetViewMatrix(scaleFactor));
	}

	// rendering
	this->Renderer.Render();
}


// -------------------------------------------------------------------------- State Change ----------------------------------------------

void GameMain::OnRestart()
{
	glm::vec2 pos = this->Level.GetPlayerStartingPosition();
	this->World.PlayerObjects[0].position_comp_->Position = pos;

	this->State = GameState::GAME_ACTIVE;
}

// -------------------------------------------------------------------------- UBO ----------------------------------------------


void GameMain::GLInitProjectionDataUBO(glm::mat4& projection, glm::mat4& view, GLfloat scale)
{
	GLsizei blockSize = 3 * sizeof(glm::mat4);

	glm::mat4 scaleMat = glm::scale(glm::mat4(), glm::vec3(scale, scale, 1.0f));

	// create buffer
	glGenBuffers(1, &this->ProjectionDataUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, this->ProjectionDataUBO);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, NULL, GL_STREAM_DRAW); // allocate 'blockSize' bytes of memory
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	
	// buffer data
	glBindBuffer(GL_UNIFORM_BUFFER, this->ProjectionDataUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(projection));
	glBufferSubData(GL_UNIFORM_BUFFER, 1 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
	glBufferSubData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(scaleMat));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// bind UBO to binding point
	glBindBufferBase(GL_UNIFORM_BUFFER, this->UBOBindings.UBOProjectionData, this->ProjectionDataUBO);
}

void GameMain::GLUpdateProjectionDataUBO(glm::mat4& projection, glm::mat4& view, GLfloat scale)
{
	glm::mat4 scaleMat = glm::scale(glm::mat4(), glm::vec3(scale, scale, 1.0f));

	glBindBuffer(GL_UNIFORM_BUFFER, this->ProjectionDataUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(projection));
	glBufferSubData(GL_UNIFORM_BUFFER, 1 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
	glBufferSubData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(scaleMat));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void GameMain::GLUpdateProjectionDataUBO(glm::mat4& view)
{
	glBindBuffer(GL_UNIFORM_BUFFER, this->ProjectionDataUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 1 * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

// -------------------------------------------------------------------------- Render Help ----------------------------------------------

void GameMain::GLUpdateResolution()
{
	// pre
	GLuint width = this->newWidth;
	GLuint height = this->newHeight;

	// body
	glViewport(0, 0, width, height);

	//glm::vec2 aspect((GLfloat)(width) / this->Width, (GLfloat)height / this->Height);
	this->Width = width;
	this->Height = height;
	this->Projection = glm::ortho(0.0f, (GLfloat)(this->Width), 0.0f, (GLfloat)(this->Height), -1.0f, 1.0f);
	
	this->Scale = (GLfloat)height / GAME_HEIGHT;

	// camera
	GLfloat worldWidthOnScreen = (GLfloat)this->Width / (UNIT_SIZE * this->Scale);
	this->World.Camera.RegularViewCorrection = glm::vec2(-worldWidthOnScreen / 2.0f, -3);
	this->World.Camera.Max = this->World.Size - glm::vec2(worldWidthOnScreen, 16);

	// parallax

	// depth = 0.0f -> world.width
	// 1.0 -> cam.width (worldWidthOnScreen)
	for (auto& var : this->World.ParallaxObjects)
	{
		auto* ppxc = reinterpret_cast<ParallaxPhysicsComponent*>(var.physics_comp_);
		var.position_comp_->Size.x = this->World.Size.x * (1 - ppxc->Depth) + worldWidthOnScreen * (ppxc->Depth);
	}

	GLfloat scaleFactor = this->Scale * this->World.Camera.Zoom * UNIT_SIZE;
	GLUpdateProjectionDataUBO(this->Projection, this->World.Camera.GetViewMatrix(scaleFactor), scaleFactor);

	this->Renderer.UpdateGLBufferedAssetResolution();

	updateResolution_ = GL_FALSE;
}
