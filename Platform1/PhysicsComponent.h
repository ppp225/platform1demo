#pragma once
#include "PositionComponent.h"
#include "InputComponent.h"
#include "RenderComponent.h"

// --------------------- Declarations ---------------------

class PlayerState;

// --------------------- Physics ---------------------

class PhysicsComponent
{
public:
	PhysicsComponent(PositionComponent& positionComponent);
	PhysicsComponent(PositionComponent& positionComponent, glm::vec4 hitbox);

	PositionComponent* PositionComponentPointer;

	const glm::vec2& GetHitboxMin();
	const glm::vec2& GetHitboxMax();
	//virtual void Update(GLfloat dt) = 0;

protected:
	// collision
	glm::vec2 min_;
	glm::vec2 max_;
};

#include "TextureStore.h"


class PlayerPhysicsComponent : public PhysicsComponent
{
public:
	PlayerPhysicsComponent(PositionComponent& positionComponent, PlayerInputComponent& playerInputComponent, RenderComponent& renderComponent);

	void Update(GLfloat dt);
	void OnPhysics(GLboolean ground, GLboolean fall);

	void UpdateTexture(GLuint textureNum, GLboolean hitboxChanged = GL_TRUE);

	// getsetters
	GLfloat GetMass() { return this->Mass; };
	void SetFrictionCoefficient(GLfloat frictionCoefficient) {
		if (this->FrictionCoefficient != frictionCoefficient) {
			this->FrictionCoefficient = frictionCoefficient;
			RecalcPrecalc();
		}
	};

	// values
	struct preCalcVal {
		GLfloat airDrag;
		GLfloat staticFriction;
		glm::vec2 gravity;
	} PrecalculatedValues;

	glm::vec2 Velocity;
	glm::vec2 prevPos;

	// const values
	// move
	const GLfloat MoveForce = 2000.0f;
	const GLfloat AirMoveForce = 250.0f;
	// land
	const GLfloat LandingFrictionMultiplier = 2.5f;
	// crouch
	const GLfloat HalfPowerTime = 1.0f;
	const GLfloat FullPowerTime = 2.0f;
	// jump
	const GLfloat JumpForceBurstValue = 6000.0f;
	const GLfloat JumpTime = 0.1f;

private:
	PlayerInputComponent* PlayerInputComponentPointer;
	RenderComponent* RenderComponentPointer;

	PlayerState* State;
	PlayerTextureStore PlayerTexture;

	// dynamic values
	GLfloat DragCoefficient = 0.01f;
	GLfloat FrictionCoefficient = 0.05f;
	// physics properties
	GLfloat Mass = 60.0f;
	glm::vec2 Gravity = glm::vec2(0.0f, -9.81f);

	// helper methods
	void HitboxChanged();
	void RecalcPrecalc();
};

class StaticPhysicsComponent : public PhysicsComponent
{
public:
	StaticPhysicsComponent(PositionComponent& positionComponent);

	GLfloat FrictionCoefficient = 0.05f;
};

class ParallaxPhysicsComponent : public PhysicsComponent
{
public:
	// *depth = layer scroll speed. 0.0f = tile layer. 0.5f = far away. 1.0f = background. 2.0f = foreground.
	ParallaxPhysicsComponent(PositionComponent& positionComponent, GLfloat depth);

	GLfloat Depth;

	void UpdatePosition(glm::vec2 camPos);
};

// --------------------- Player State --------------------- --------------------- --------------------- --------------------- ---------------------

/* *** Hierarchy ***

class PlayerState		//	0

class GroundedState;	//		1
class StandingState;	//			2
class MoveRightState;	//			2
class MoveLeftState;	//			2
class LandingState;		//			2
class CrouchingState;	//			2

class AerialState;		//		1
class JumpingState;		//			2
class RisingState;		//			2
class FallingState;		//			2


*/

class PlayerState
{
	friend class PlayerPhysicsComponent;

protected:
	const GLboolean DEBUG = GL_FALSE;

public:
	enum class StateID {
		UNDEF,
		GROUND,
		AERIAL,
	};
	enum class StateSubID {
		UNDEF,

		LANDING,
		CROUCHING,

		JUMPING,
	};
	StateID ID;
	StateSubID SubID;

public:
	PlayerState(GLboolean lookRight) : lookRight_(lookRight) {};
	virtual ~PlayerState() = default;
	//* called when entering state
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent) = 0;
	//* called when updating player state. Velocity calculated using 'Force' and 'Friction' methods.
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input) = 0;

protected:
	GLboolean lookRight_;

private:
	virtual glm::vec2 Force(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input) = 0;
	virtual glm::vec2 Friction(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input) = 0;
};

// --------------------- State : Grounded ---------------------

class GroundedState : public PlayerState
{
public:
	GroundedState(GLboolean lookRight) : PlayerState(lookRight) {};
	virtual ~GroundedState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent) = 0;
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input) = 0;

private:
	virtual glm::vec2 Force(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
	virtual glm::vec2 Friction(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};

class StandingState : public GroundedState
{
public:
	StandingState(GLboolean lookRight) : GroundedState(lookRight) {};
	virtual ~StandingState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);

private:
	GLuint stateNum_ = 0;
	GLfloat idleTime_ = 0.0f;
	GLfloat moveTime_ = 0.0f;
};

class MoveRightState : public GroundedState
{
public:
	MoveRightState(GLboolean lookRight) : GroundedState(lookRight) {};
	virtual ~MoveRightState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);

private:
	GLfloat startPos_;
};

class MoveLeftState : public GroundedState
{
public:
	MoveLeftState(GLboolean lookRight) : GroundedState(lookRight) {};
	virtual ~MoveLeftState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);

private:
	GLfloat startPos_;
};

class LandingState : public GroundedState
{
public:
	LandingState(GLboolean lookRight) : GroundedState(lookRight) {};
	virtual ~LandingState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);

private:
	GLboolean strongSideways_;
	GLfloat time_;
	virtual glm::vec2 Force(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
	virtual glm::vec2 Friction(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};

class CrouchingState : public GroundedState
{
public:
	CrouchingState(GLboolean lookRight) : GroundedState(lookRight) {};
	virtual ~CrouchingState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);

private:
	GLfloat time_ = 0.0f;

	// onEnter
	GLfloat halfPowerTime_;
	GLfloat fullPowerTime_;

	virtual glm::vec2 Force(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
	virtual glm::vec2 Friction(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};

// --------------------- State : Aerial ---------------------

class AerialState : public PlayerState
{
public:
	AerialState(GLboolean lookRight) : PlayerState(lookRight) {};
	virtual ~AerialState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent) = 0;
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input) = 0;

private:
	virtual glm::vec2 Force(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
	virtual glm::vec2 Friction(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};

class JumpingState : public AerialState
{
public:
	JumpingState(GLboolean lookRight) : AerialState(lookRight), jumpForceBurstValue_(0.0f), totalJumpTime_(0.0f) {};
	JumpingState(GLboolean lookRight, GLfloat burstValue, GLfloat jumpTime) : AerialState(lookRight), jumpForceBurstValue_(burstValue), totalJumpTime_(jumpTime) {};
	virtual ~JumpingState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);

private:
	GLfloat jumpTime_ = 0.0f;
	GLfloat forceSeconds_;

	// constructor / onEnter
	GLfloat jumpForceBurstValue_;
	GLfloat totalJumpTime_;

	virtual glm::vec2 Force(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};

class RisingState : public AerialState
{
public:
	RisingState(GLboolean lookRight) : AerialState(lookRight) {};
	virtual ~RisingState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};

class FallingState : public AerialState
{
public:
	FallingState(GLboolean lookRight) : AerialState(lookRight) {};
	virtual ~FallingState() = default;
	virtual void OnEnter(PlayerPhysicsComponent& physicsComponent);
	virtual PlayerState* Update(PlayerPhysicsComponent& physicsComponent, GLfloat dt, PlayerInput& input);
};