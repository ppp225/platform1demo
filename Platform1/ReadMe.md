-------------------------------------------------------------------------------

# Platform1: Simple C++ & OpenGL **Game Engine** for a 2D Platform Game

-------------------------------------------------------------------------------
## Common game controls

Keys | Action
----------|--------
WSAD | Movement
Ctrl | Charge jump (press "W" for strong jump while still holding)
F5 | Restart game
F11 | Fullscreen
ESC | Exit game

-------------------------------------------------------------------------------

## Tools used

* **C++** 11+ - for everything.
* **OpenGL** 3.3 - for graphics.
* **GLFW** + GLEW - for windowing.
* **SOIL2** - for image loading from disk and creating OpenGL textures.
* **glm** - for math things.

-------------------------------------------------------------------------------

## Code description

### Input Handling:
* `ObserverWorks.h` - implementation of **Observer** pattern.
  Currently only `KeyInputObserver` used.
	* Subjects reside in the `Windowing` class, where callbacks notify them.
	  Pointers are kept in the `GameMain` class, as observers need to be registered with them.
	* Observers reside in those classes:
		* `InputComponent` - player related key capture (i.e. movement)
		* `SettingExecutor` - game related key capture (i.e. settings)
		* Key input is also handled in the `Windowing::HandleKeys()` method
		  for windowing related things, like closing the window or fullscreen.

### Entity organization:
* `GameEntities.h`
	* class `GameObject` - generic GameObject class using **Component** pattern, holds components:
		* `PositionComponent` - Holds object position data.
		* `PhysicsComponent` - Holds physical properties and object behavior.
		  Detailed explanation below in the "physics" section.
		* `RenderComponent` - Holds data needed for rendering the object.
		* `InputComponent` - Handles player input / NPC AI.
	* class `TheWorld` - contains all current objects related to the game world.
	* class `GameLevel` - relic from previous iteration,
	  reads gamelevel data from file,
	  TODO: needs to be remade into a static GameLevelLoader


### Physics:
* `PhysicsEngine`
	* Simple collision detection and resolution between objects.
* `PhysicsComponent`
	* This component is responsible for physics unrelated to other objects.
	  (like changing the player state, calculating jumping variables etc.)
	* At line 113 starts the implementation of the **State** pattern
	  for player behavior/animation. (Line 113 in `PhysicsComponent.h`)
	* Is used in the `PlayerPhysicsComponent` class, mainly `Update()` method.

### Rendering:
* `RenderComponent`
	* Describes entities (the used texture, coloring etc.)
* `RenderEngine`
	* Reads and manages component data in it's own structures.
* `SpriteRenderer`
	* interfaces with OpenGL.
* `UBOBindingWorks`, `Texture2D`, `Shader`
	* Helper classes for OpenGL things.

-------------------------------------------------------------------------------

## Other class files:

#### `GameMain`
* Class that ties the game together. Game loop, basic game states etc.

#### `TileTools`
* Used by the GameEntities/GameLevel class to determine the current tile
  position relative to other tiles, and chose it's graphics accordingly.
	
#### `Windowing`
* Manages the window creation, input capture and all window related things.
* Also includes OpenGL-window related things like buffer swapping and game loop execution.

#### `TextureStore`, `SettingStore`
* Stores information about used textures and settings.

#### `Camera`
* Very simple camera.

-------------------------------------------------------------------------------

## Other standard files:

#### `StdAfx.h/.cpp`
* These files are used to build a precompiled header (PCH) file
  named Platform1.pch and a precompiled types file named StdAfx.obj.

#### `Platform1.cpp`
* Application entry point

-------------------------------------------------------------------------------
