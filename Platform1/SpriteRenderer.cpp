#include "stdafx.h"
#include "SpriteRenderer.h"

// ---------------------------------------------------------------------------------------- Instanced ----------------------------------------------------------------------------------------

void InstancedRenderer::Delete()
{
	glDeleteVertexArrays(1, &this->VAO);
	this->InShader.Delete();
	glDeleteBuffers(1, &this->ConstantsUBO);
}

void InstancedRenderer::Init(Shader& shader, UBOBindingWorks& uboBindings, std::vector<glm::vec3>& instanceData, Texture2D& texture, glm::ivec2& texDivisor, glm::vec4& color)
{
	this->InShader = shader;
	this->Texture = &texture;

	InitRenderData(instanceData);
	InitConstantsUBO(color, texDivisor, uboBindings);
	BindMVPMatricesUBO(uboBindings);

	this->ObjectsNum = instanceData.size();
}

void InstancedRenderer::DrawInstanced()
{
	this->InShader.Use();

	glActiveTexture(GL_TEXTURE0);
	this->Texture->Bind();

	glBindVertexArray(this->VAO);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 6, this->ObjectsNum);
	glBindVertexArray(0);
}

void InstancedRenderer::InitRenderData(std::vector<glm::vec3>& instanceData)
{
	GLfloat vertices[] = {
		// Pos      // Tex
		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,

		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f
	};

	// generate
	glGenVertexArrays(1, &this->VAO);
	GLuint VBO[2];
	glGenBuffers(2, VBO);

	// bind
	glBindVertexArray(this->VAO);

	// buffer [0]
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// buffer [1] instanced
	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * instanceData.size(), &instanceData[0], GL_STATIC_DRAW);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribDivisor(1, 1); // Tell OpenGL this is an instanced vertex attribute.

	// unbind and cleanup
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//glDeleteBuffers(2, VBO);
}

void InstancedRenderer::InitConstantsUBO(glm::vec4 color, glm::ivec2 texDivisor, UBOBindingWorks& uboBindings)
{
	GLsizei blockSize = sizeof(glm::vec4) + sizeof(glm::vec2);	// look into shader for details

	// create buffer
	glGenBuffers(1, &this->ConstantsUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, this->ConstantsUBO);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, NULL, GL_STATIC_DRAW); // allocate 'blockSize' bytes of memory
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// store data
	glBindBuffer(GL_UNIFORM_BUFFER, this->ConstantsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec4), glm::value_ptr(color));
	glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::vec4), sizeof(glm::vec2), glm::value_ptr(texDivisor));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// link shader uniform block to uniform binding point
	GLuint bindingPoint = uboBindings.GetFreeBinding();
	this->InShader.SetUniformBlockBinding("Constants", bindingPoint);
	// Define the range of the buffer that links to a uniform binding point
	glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, this->ConstantsUBO);
	//glBindBufferRange(GL_UNIFORM_BUFFER, 0, this->ConstantsUBO, 0, blockSize);
}

void InstancedRenderer::BindMVPMatricesUBO(UBOBindingWorks& uboBindings)
{
	this->InShader.SetUniformBlockBinding("ProjectionData", uboBindings.UBOProjectionData);
}

// ---------------------------------------------------------------------------------------- Dynamic ----------------------------------------------------------------------------------------

void DynamicRenderer::Delete()
{
	glDeleteVertexArrays(1, &this->VAO);
	this->DyShader.Delete();
	glDeleteBuffers(1, &this->ConstantsUBO);
	glDeleteBuffers(1, &this->DynamicsUBO);
}

void DynamicRenderer::Init(Shader& shader, UBOBindingWorks& uboBindings, Texture2D& texture, glm::ivec2& texDivisor, glm::vec4& color)
{
	this->DyShader = shader;
	this->Texture = &texture;

	InitRenderData();
	InitConstantsUBO(texDivisor, uboBindings);
	InitDynamicsUBO(color, glm::vec2(), glm::vec2(), 0, uboBindings);	// TODO:
	BindMVPMatricesUBO(uboBindings);
}

void DynamicRenderer::UpdateData(glm::vec2& position, GLuint texNum)
{
	// buffer data
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 16, sizeof(glm::vec2), glm::value_ptr(position));
	glBufferSubData(GL_UNIFORM_BUFFER, 32, sizeof(GLuint), &texNum);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void DynamicRenderer::UpdateData(glm::vec4& color, glm::vec2& position, glm::vec2& size, GLuint texNum)
{
	// buffer data
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec4), glm::value_ptr(color));
	glBufferSubData(GL_UNIFORM_BUFFER, 16, sizeof(glm::vec2), glm::value_ptr(position));
	glBufferSubData(GL_UNIFORM_BUFFER, 24, sizeof(glm::vec2), glm::value_ptr(size));
	glBufferSubData(GL_UNIFORM_BUFFER, 32, sizeof(GLuint), &texNum);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void DynamicRenderer::Draw()
{
	this->DyShader.Use();

	glActiveTexture(GL_TEXTURE0);
	this->Texture->Bind();

	glBindVertexArray(this->VAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

void DynamicRenderer::InitRenderData()
{
	GLfloat vertices[] = {
		// Pos      // Tex
		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,

		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f
	};

	// generate
	glGenVertexArrays(1, &this->VAO);
	GLuint VBO;
	glGenBuffers(1, &VBO);

	// bind
	glBindVertexArray(this->VAO);

	// buffer [0]
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	 // unbind and cleanup
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//glDeleteBuffers(1, &VBO);
}

void DynamicRenderer::InitConstantsUBO(glm::ivec2& texDivisor, UBOBindingWorks& uboBindings)
{
	GLsizei blockSize = sizeof(glm::vec2);	// look into shader for details

	// create buffer
	glGenBuffers(1, &this->ConstantsUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, this->ConstantsUBO);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, NULL, GL_STATIC_DRAW); // allocate 'blockSize' bytes of memory
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// store data
	glBindBuffer(GL_UNIFORM_BUFFER, this->ConstantsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec2), glm::value_ptr(texDivisor));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// link shader uniform block to uniform binding point
	GLuint bindingPoint = uboBindings.GetFreeBinding();
	this->DyShader.SetUniformBlockBinding("Constants", bindingPoint);
	// Define the range of the buffer that links to a uniform binding point
	glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, this->ConstantsUBO);
	//glBindBufferRange(GL_UNIFORM_BUFFER, 0, this->ConstantsUBO, 0, blockSize);
}

void DynamicRenderer::InitDynamicsUBO(glm::vec4& color, glm::vec2& position, glm::vec2& size, GLuint texNum, UBOBindingWorks& uboBindings)
{
	GLsizei blockSize = 36;	// look into shader for details

	// create buffer
	glGenBuffers(1, &this->DynamicsUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, NULL, GL_DYNAMIC_DRAW); // allocate 'blockSize' bytes of memory
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// store data
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec4), glm::value_ptr(color));
	glBufferSubData(GL_UNIFORM_BUFFER, 16, sizeof(glm::vec2), glm::value_ptr(position));
	glBufferSubData(GL_UNIFORM_BUFFER, 24, sizeof(glm::vec2), glm::value_ptr(size));
	glBufferSubData(GL_UNIFORM_BUFFER, 32, sizeof(GLint), &texNum);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// link shader uniform block to uniform binding point
	GLuint bindingPoint = uboBindings.GetFreeBinding();
	this->DyShader.SetUniformBlockBinding("Dynamics", bindingPoint);
	// Define the range of the buffer that links to a uniform binding point
	glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, this->DynamicsUBO);
	//glBindBufferRange(GL_UNIFORM_BUFFER, 0, this->ConstantsUBO, 0, blockSize);
}

void DynamicRenderer::BindMVPMatricesUBO(UBOBindingWorks& uboBindings)
{
	this->DyShader.SetUniformBlockBinding("ProjectionData", uboBindings.UBOProjectionData);
}

// ---------------------------------------------------------------------------------------- Parallax  ----------------------------------------------------------------------------------------

void ParallaxRenderer::Delete()
{
	glDeleteVertexArrays(1, &this->VAO);
	this->PaShader.Delete();
	glDeleteBuffers(1, &this->DynamicsUBO);
}

void ParallaxRenderer::Init(Shader & shader, UBOBindingWorks & uboBindings, Texture2D & texture)
{
	this->PaShader = shader;
	this->Texture = &texture;

	InitRenderData();
	InitDynamicsUBO(uboBindings);
	BindMVPMatricesUBO(uboBindings);
}

void ParallaxRenderer::UpdateData(glm::vec2 & position)
{
	// buffer data
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 16, sizeof(glm::vec2), glm::value_ptr(position));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void ParallaxRenderer::UpdateData(glm::vec4 & color, glm::vec2 & position, glm::vec2 & size)
{
	// buffer data
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec4), glm::value_ptr(color));
	glBufferSubData(GL_UNIFORM_BUFFER, 16, sizeof(glm::vec2), glm::value_ptr(position));
	glBufferSubData(GL_UNIFORM_BUFFER, 24, sizeof(glm::vec2), glm::value_ptr(size));
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void ParallaxRenderer::Draw()
{
	this->PaShader.Use();

	glActiveTexture(GL_TEXTURE0);
	this->Texture->Bind();

	glBindVertexArray(this->VAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

void ParallaxRenderer::InitRenderData()
{
	GLfloat vertices[] = {
		// Pos      // Tex
		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f,

		0.0f, 1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 0.0f
	};

	// generate
	glGenVertexArrays(1, &this->VAO);
	GLuint VBO;
	glGenBuffers(1, &VBO);

	// bind
	glBindVertexArray(this->VAO);

	// buffer [0]
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	// unbind and cleanup
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//glDeleteBuffers(1, &VBO);
}

void ParallaxRenderer::InitDynamicsUBO(UBOBindingWorks & uboBindings)
{
	GLsizei blockSize = 32;	// look into shader for details

	// create buffer
	glGenBuffers(1, &this->DynamicsUBO);
	glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, NULL, GL_DYNAMIC_DRAW); // allocate 'blockSize' bytes of memory
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// store data
	//glBindBuffer(GL_UNIFORM_BUFFER, this->DynamicsUBO);
	//glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::vec4), glm::value_ptr(color));
	//glBufferSubData(GL_UNIFORM_BUFFER, 16, sizeof(glm::vec2), glm::value_ptr(position));
	//glBufferSubData(GL_UNIFORM_BUFFER, 24, sizeof(glm::vec2), glm::value_ptr(size));
	//glBindBuffer(GL_UNIFORM_BUFFER, 0);

	// link shader uniform block to uniform binding point
	GLuint bindingPoint = uboBindings.GetFreeBinding();
	this->PaShader.SetUniformBlockBinding("Dynamics", bindingPoint);
	// Define the range of the buffer that links to a uniform binding point
	glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, this->DynamicsUBO);
	//glBindBufferRange(GL_UNIFORM_BUFFER, 0, this->ConstantsUBO, 0, blockSize);
}

void ParallaxRenderer::BindMVPMatricesUBO(UBOBindingWorks & uboBindings)
{
	this->PaShader.SetUniformBlockBinding("ProjectionData", uboBindings.UBOProjectionData);
}
