#include "stdafx.h"
#include "PositionComponent.h"

// ---------------------------------------------------------------------------------------- Position Component ----------------------------------------------------------------------------------------

PositionComponent::PositionComponent(glm::vec2 position, glm::vec2 size) : Position(position), Size(size)
{
}

