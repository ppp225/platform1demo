#version 330 core
in vec2 TexCoords;
in vec4 SpriteColor;

// textures, 0 is closest. max. 16.
uniform sampler2D texture0;
// uniform sampler2D texture1;
// uniform sampler2D texture2;

out vec4 color;

void main()
{
    color = (texture(texture0, TexCoords) * SpriteColor);
}