#version 330 core
in vec2 TexCoords;
in vec4 SpriteColor;

flat in int TexNum;
flat in ivec2 TexDivisor;

uniform sampler2D texture0;

out vec4 color;

void main()
{
	int cols = TexDivisor.x;
	int rows = TexDivisor.y;
	int col = TexNum / rows;
	int row = TexNum - col * rows;
	float rowOffset = 1.0f / rows;
	float colOffset = 1.0f / cols;
	vec2 shift = vec2(col * colOffset, row * rowOffset);
	
	vec2 finalTexCoords = TexCoords * vec2(colOffset - 0.000001f, rowOffset - 0.000001f) + (shift + 0.000002f);
	
    color = (texture(texture0, finalTexCoords) * SpriteColor);
}