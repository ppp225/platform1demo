#version 330 core
layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>

layout (std140) uniform ProjectionData
{
							// base alignment	// position in memory
	mat4 projection;		// 64				// 0
	mat4 view;				// 64				// 64
	mat4 scale;				// 64				// 128
												// total size: 192
};

layout (std140) uniform Constants
{
	//						// base alignment	// position in memory
    ivec2 texDivisor;		// 8				// 0
												// total size: 8
};

layout (std140) uniform Dynamics
{
	//						// base alignment	// position in memory
	vec4 spriteColor;		// 16				// 0
    vec2 position;			// 8				// 16
    vec2 size;				// 8				// 24
    int texNum;				// 4				// 32
												// total size: 36
};

out vec2 TexCoords;
out vec4 SpriteColor;
flat out int TexNum;
flat out ivec2 TexDivisor;

void main()
{
    gl_Position = projection * view * scale * vec4(vertex.xy * size + position, 0.0f, 1.0f); 
    TexCoords = vertex.zw;
	SpriteColor = spriteColor;
	TexNum = texNum;
	TexDivisor = texDivisor;
}